﻿Imports System.Web
Imports System.Web.Optimization

Public Module BundleConfig
    ' Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
    Public Sub RegisterBundles(ByVal bundles As BundleCollection)
        bundles.Add(New ScriptBundle("~/bundles/angular").Include(
                  "~/Static/angular/angular.js",
                  "~/Static/angular/angular-route.js",
                  "~/Scripts/config/app.js"
        ))

        bundles.Add(New StyleBundle("~/Content/css").Include(
                  "~/Static/bootstrap/bootstrap.css",
                  "~/Content/Styles/site.css"))

        bundles.Add(New ScriptBundle("~/bundles/jquery").Include(
                    "~/Static/jquery/jquery-{version}.js"))

        bundles.Add(New ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Static/jquery/jquery.validate*"))

        ' Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
        ' para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
        bundles.Add(New ScriptBundle("~/bundles/modernizr").Include(
                    "~/Static/modernizr-*"))

        bundles.Add(New ScriptBundle("~/bundles/bootstrap").Include(
                  "~/Static/bootstrap/bootstrap.js"))
    End Sub
End Module