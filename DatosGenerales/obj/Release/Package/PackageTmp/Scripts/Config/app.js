﻿var app = angular.module('myApp', ['ngMaterial', 'ngAria', 'ngAnimate','ngRoute','oc.lazyLoad','ui.router']);

app.component('tmkGraficaBarras', {
  transclude: false,
  bindings: {
	  idGrafica: '@',
	  data: '=',
	  ejes: '=',
	  settings: '=',
	  series:'='
  },
  controller: function() {
	var vm = this;
	console.log(vm);
	vm.$onInit = function() {
	  vm.chartAmchart = null;
	  var config = {
		  colorCirculoLinea : "#fff",
		  barrasSeparadas:false,
		  posicionLeyenda:"top"
	  };

	  var coloresAts = ["#dac5c7", "#F4343E", "#5B5B62", "#CA939C", "#666"],
		  coloresTlmark = ["#87c3ff", "#005dba", "#2190ff", "#e30052", "#666", "#a160a0", "#eb6eb0", "#f597bb", "#fbb8c9", "#f8d4d8"],
		  colores = vm.settings.theme == "AST" ? coloresAts : coloresTlmark;

	  vm.chartAmchart = am4core;
	  vm.chartAmchart.ready(function() {
		  // Themes begin

		  vm.chartAmchart.useTheme(am4themes_animated);
		  // Themes end

		  // Create chart instance

		  var chart = vm.chartAmchart.create(vm.idGrafica, am4charts.XYChart);

		  // Add data
		  chart.data = vm.data;

		  // Create axes
		  if (vm.settings.tipoFecha) {
			  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
			  // datos en cada columna
			  dateAxis.renderer.grid.template.location = 0;
			  dateAxis.renderer.minGridDistance = 30;

			  var ejesVerticales = {};
			  vm.ejes.forEach(function(element, i) {
				  var ejeAuxiliar = chart.yAxes.push(new am4charts.ValueAxis());
				  ejeAuxiliar.title.text = element;
				  if (i == 1) {
					  ejeAuxiliar.renderer.opposite = true;
					  ejeAuxiliar.renderer.grid.template.disabled = true;
				  }
				  ejesVerticales[element] = ejeAuxiliar;
			  });
		  }else {
			  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
			  categoryAxis.renderer.grid.template.location = 0;
			  categoryAxis.dataFields.category = vm.ejes[0];
			  categoryAxis.renderer.minGridDistance = 40;
			  categoryAxis.fontSize = 11;
              categoryAxis.renderer.inside = true;
			  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
			  // valueAxis.min = 0;
			  // valueAxis.max = 24000;
			  // valueAxis.strictMinMax = false;
			  valueAxis.renderer.minGridDistance = 30;
		  }


		  var seriesScrollX = [];

		  vm.series.forEach(function(element, i) {
			  var series1;
			  if (element.linea) {
				  series1 = chart.series.push(new am4charts.LineSeries());
				  series1.dataFields.valueY = element.nombreValorY;
				  console.log('tipoFecha');
				  if (vm.settings.tipoFecha) {
					  series1.dataFields.dateX = element.nombreValorX;
				  }else {
					  series1.dataFields.categoryX = element.nombreValorX;
				  }
				  series1.yAxis = ejesVerticales[element.nombreEjeY];
				  series1.name = element.texto;
				  series1.tooltipText = "{name}\n[bold font-size: 20]{valueY}[/]";
				  series1.strokeWidth = 2;
				  series1.tensionX = element.linea.curvaturaX || 0.7;
				  if (element.color) {
					  series1.stroke = element.color;
					  series1.fill = element.color;
				  }else if (i <= colores.length) {
					  series1.stroke = colores[i];
					  series1.fill = colores[i];
				  }else {
					  series1.stroke = colores[colores.length -1];
					  series1.fill = colores[colores.length -1];
				  }

				  var bullet3 = series1.bullets.push(new am4charts.CircleBullet());
				  bullet3.circle.radius = 3;
				  bullet3.circle.strokeWidth = 2;
				  bullet3.circle.fill = am4core.color( element.linea.colorCirculoLinea || config.colorCirculoLinea);
				  if (element.linea.punteada) {
					  series1.strokeDasharray = "3,3";
				  }
			  }else {
				  series1 = chart.series.push(new am4charts.ColumnSeries());
				  series1.dataFields.valueY = element.nombreValorY;
				  // series1.dataFields.dateX = element.nombreValorX;
				  series1.name = element.texto;
				  if (vm.settings.tipoFecha) {
					  series1.dataFields.dateX = element.nombreValorX;
					  series1.tooltipText = "{name}\n[bold font-size: 20]{valueY}[/]";
					  series1.yAxis = ejesVerticales[element.nombreEjeY];
				  }else {
					  series1.dataFields.categoryX = element.nombreValorX;
					  series1.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
				  }
				  if (element.width)
					  series1.columns.template.width = am4core.percent(element.width);
				  if (element.color) {
					  series1.fill = element.color;
				  }else if (i <= colores.length) {
					  series1.fill = colores[i];
				  }else {
					  series1.fill = colores[colores.length -1];
				  }
				  // if (element.color)
				  // 	series1.fill = element.color;
				  series1.strokeWidth = 0;
				  series1.clustered = element.barrasSeparadas || config.barrasSeparadas;
			  }
			  if (element.scrollX) {
				  seriesScrollX.push(series1);
			  }
		  });



		  if (seriesScrollX.length) {
			  chart.scrollbarX = new am4charts.XYChartScrollbar();
			  seriesScrollX.forEach(function(element) {
				  chart.scrollbarX.series.push(element);
			  })
			  chart.scrollbarX.parent = chart.bottomAxesContainer;
		  }

		  // Add cursor
		  chart.cursor = new am4charts.XYCursor();

		  // Add legend
		  chart.legend = new am4charts.Legend();
		  // posicion de los titulos
		  chart.legend.position = vm.settings.posicionLeyenda || config.posicionLeyenda;


	  }); // end am4core.ready()


	};
}
});
