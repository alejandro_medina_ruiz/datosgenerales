﻿app.controller('main', function ($scope,$rootScope, $http, $mdSidenav, $mdToast, $timeout, $log, $mdDialog, $compile,$element,
    themeFactory, asyncFactory
    ) {
    $scope.dominio = dominio;
    //TLMARK || AST || B12
    $scope.empresa = empresa;
    $rootScope.isAdmin = localStorage.getItem("admin") == 'true';
    //$scope.empresa = "AST";//DEBUG
    $scope.loading = false;
    $scope.theme = themeFactory.getTheme($scope.empresa);
    for (var i = 0; i < $scope.theme.ambientes.length; i++) {
        if ($scope.theme.ambientes[i].selected) {
            $scope.ambiente = $scope.theme.ambientes[i];
        }
    }

    $scope.init = function () {
        $scope.loading = true;
        $http({
            method: 'GET',
            url: dominio + "api/Work/getComponents"
        })
        .then(function (response) {
            asyncFactory.load(response.data.value, "settings", function () {
                $http({
                    url: "api/Work/getHome",
                    method: 'GET',
                    headers: {
                        'AuthorizationTMK': localStorage.getItem("token")
                    }
                })
                .then(function (response) {
                    usuario = JSON.parse(response.data.usuario);
                    console.log('usuario');
                    console.log(usuario);
                    if (
                        ( usuario.rol == 'RESPONSABLES' && ( usuario.proyecto == 'MOVISTAR_FIJA' || usuario.proyecto == 'HIBU' || usuario.proyecto == 'MOVISTAR_DIGITAL' ) )
                        || usuario.usuario.toLowerCase().includes("tec") )
                    {
                        console.log(usuario);
                        if ( usuario.usuario.toLowerCase().includes("tec") ) {
                            usuario.rol = 'TECNICOS'
                            usuario.proyecto = 'MOVISTAR_FIJA';
                        }
                        asyncFactory.load(response.data.value, "contenedor", function () {
                            //console.log("procesado");
                            $scope.loading = false;
                        });
                    }else {
                        if (usuario.rol == 'SUPERVISORES' && usuario.usuario == 'sup05jv2') {
                            usuario.proyecto = 'MOVISTAR_FIJA';
                            asyncFactory.load(response.data.value, "contenedor", function () {
                                //console.log("procesado");
                                $scope.loading = false;
                            });
                        }else {
                            throw "Usuario no valido"
                        }
                    }
                }).catch(function (e) {
                    console.log(e);
                    $http.get(dominio + "api/Work/getError" ).then(function (r) {
                        asyncFactory.load(r.data.value, "contenedor", function () {
                            $scope.loading = false;
                        });
                    });

                });
              });
          });

        // });


    }

    $scope.init();

    $rootScope.apiRun = function (req) {
        $scope.loading = true;
        return new Promise((aceptar, cancelar) => {
            var paramsSt

            if (req.params !== undefined) {
                paramsSt = "?"
            } else {
                paramsSt = ""
            }

            var first = true;
            for (k in req.params) {
                if(!first){
                    paramsSt += "&"
                }
                else{
                    first = false;
                }
                paramsSt += k + "=" + req.params[k]
            }

            $http({
                method: req.method?req.method:"POST",
                url: dominio + req.url + paramsSt,
                headers: {
                    'AuthorizationTMK': localStorage.getItem("token")
                },
                data: req.data
            }).then(function (r) {
                if (!r.data.codeValkiriaError) {
                    aceptar(r);
                } else {
                    if (r.data.codeValkiriaError == "001") {
                        $http.get(dominio + "api/Work/getLogging").then(function (r) {
                            asyncFactory.load(r.data.value, "contenedor", function () {
                                //console.log("procesado");
                                $scope.loading = false;
                            });
                        });
                    }
                }

                $scope.loading = false;

            }).catch(function (e) {
                cancelar(e);
                if (e.status == 426) {
                    $http.get(dominio + "api/Work/getLogging").then(function (r) {
                        asyncFactory.load(r.data.value, "contenedor", function () {
                            //console.log("procesado");
                            $scope.loading = false;
                        });
                    });
                } else {
                    console.log(e);
                    $rootScope.error = e;
                    $scope.loading = false;
                }
            });
        })
    }
});
