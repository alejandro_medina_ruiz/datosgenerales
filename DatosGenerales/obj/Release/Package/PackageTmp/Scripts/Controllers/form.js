// app.controller('formCtrl', function ($scope) {
//     $scope.message = 'Hello from Form Controller';
//     $scope.form = {};
// });

app.controller('formCtrl', ['$scope', '$http', '$timeout', '$rootScope', 'themeFactory',  function ($scope, $http, $timeout, $rootScope, themeFactory) {

    $scope.theme = themeFactory.getTheme(empresa);

    // arreglo para select
    $scope.observaciones = [
      { dynamic:true, text: 'CLIENTE TERCERA EDAD'},
      { dynamic:true, text: 'INDISPONIBILIDAD ATIS'},
      { dynamic:true, text: 'CLIENTE EXIGE VISITA'},
      { dynamic:true, text: 'PQR'},
      { dynamic:false, text: 'CLIENTE RECURRENTE'},
      { dynamic:true, text: 'TRASLADO INTERNO FIBRA ÓPTICA'},
      { dynamic:true, text: '3er CONTACTO ASSIA RECOMIENDA AVERIA'},
      { dynamic:true, text: '3er CONTACTO NO SINCRONIZA +48 HORAS'},
      { dynamic:false, text: 'BASE GENERAR AVERÍA'},
      { dynamic:false, text: 'SOLICITUD EXPRESA POR CORREO ELECTRONICO (CLIENTE TELEFÓNICA)'},
      { dynamic:false, text: 'IMPOSIBILIDAD MIGRACIÓN FTTH.'}
    ];

    // arreglo para radiobuttons
    $scope.opcionesDireccion = [
        { value: 1, name: "Intraducible" }, { value: 2, name: "Barrio - Manzana" }, { value: 3, name: "Calle - Carrera" }
    ];

    // Funcion para change de selects
    $scope.cambiar = function() {
      console.log($scope.carro);
    };
    // arreglos para selects

    $scope.form = {};

    $scope.initCtr = function () {

    };


    /**
     * @name _validar
     * @desciption Esta variable tipo JSON se utiliza para validar el formulario,
	 *       como claves se usa el nombre de los campos que queremos validar,
	 *       como valor se pasan los parametros que queremos validar (presence, length, format, email etc)
     *
     */
    var _validar = {
      numerico: {
        presence: {
          value:true,
          message: "^El campo 'Númerico' es requerido."
        },
        length: {
          minimum: 8,
          maximum: 8,
          message: "^El campo 'Númerico' Debe tener 8 dígitos."
        },
        format: {
          pattern: /^[0-9]+$/,
          message: "^No se permiten caracteres especiales"
        }
      },
      texto: {
        presence: {
          value:true,
          message: "^El campo 'Texto' es requerido."
        }
      },
      observacion: {
        presence: {
          value:true,
          message: "^El campo 'Observación' es requerido."
        }
      },
      email: {
        presence: {
          value:true,
          message: "^El campo 'Email' es requerido."
        },
        email: {
          value:true,
          message: "^El campo 'Email' no es correcto"
        }
      },
    };

    /**
     * @name guardar
     * @desciption Esta Función nos permitira guardar una vez sea valido nuestro formulario,
     *       para validar basta con pasar a la funcion validate (de la librería validate.js) como parametros
     *       nuestro scope donde esten los campos que para este ejemplo esta en la variable form ($scope.form),
     *       y como segundo parametro la variable que tiene las validaciones, en este ejemplo _validar, esta función retorna
     *       los errores que hay en el formulario
     *
     */
    $scope.guardar = function () {
      $scope.errores = validate($scope.form, _validar);
      console.log('$scope.errores');
      console.log($scope.errores);
      if ( !$scope.errores ) {
        var form = $scope.form;
        form.idAgente = form.idAgente.id;
        if (!form.observacion.dynamic) { form.casDynamics = ""; }
        form.observacion = form.observacion.text;
        if (!$scope.editar) { form.fechaRegistro = new Date(); };
        $rootScope.apiRun({ url: "api/Ejemplo/Guardar", method: "POST", data: { form: JSON.stringify(form) } })
        .then(function (respuesta) {
          $scope.form = {};
          $scope.CloseDialog();
          $scope.$apply();
        })
        .catch(function (err) {
          $scope.toaster(err.data.message, 7, false, true);
          console.log(err);
          $scope.$apply();
        });
      }

    };

    $scope.initCtr();
  }]);
