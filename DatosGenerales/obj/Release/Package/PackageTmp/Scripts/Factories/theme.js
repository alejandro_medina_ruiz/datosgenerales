﻿app.factory("themeFactory", function () {
    var theme = {
        backgroundThemeColor: "#858585",
        backgroundColor: "#000000",
        backgroundLoading: "#1a1a1a",
        corpImage: dominio + "Content/Images/coorp/iconoTlmark.PNG",
        corpImage2: dominio + "Content/Images/coorp/logotlmark.png",
        displayName: "Tlmark",
        fontColor: "#FFFFFF",
        ambientes: [
            {
                backgroundNormal: "#FFFBFF",
                backgroundGeneral: "#BFBDBF",
                backgroundImportante: "#403F40",
                fontColorDiscreto: "#7F7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#FFFFFF",
                selected: true
            },
            {
                backgroundNormal: "#FAFDFF",
                backgroundGeneral: "#41617F",
                backgroundImportante: "#B8DDFF",
                fontColorDiscreto: "#7D7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FAFFFE",
                backgroundGeneral: "#427F6E",
                backgroundImportante: "#B8FFEA",
                fontColorDiscreto: "#7D7F7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FFFFFA",
                backgroundGeneral: "#7D7F41",
                backgroundImportante: "#FDFFB8",
                fontColorDiscreto: "#7F7F7D",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            }
        ]
    }
    var tlmark = {
        backgroundThemeColor: "#FFFFFF",
        backgroundColor: "#404040",
        backgroundSideThemeColor: "#959595",
        backgroundSideOverThemeColor: "#666666",
        backgroundLoading: "#1a1a1a",
        backgroundLogin: 'Content/Images/coorp/FONDOS_LOGIN/TLMARK.png',
        corpImage: dominio + "Content/Images/coorp/tlmarkfondosnegros.png",
        corpImage2: dominio + "Content/Images/coorp/logotlmark.png",
        displayName: "Tlmark",
        fontColor: "#FFFFFF",
        buttonColor: "#F15A24",
        fontRegular: "tlmarkAst-regular",
        fontBold: "tlmarkAst-bold",
        fontLight: "tlmarkAst-light",
        ambientes: [
            {
                backgroundNormal: "#FFFBFF",
                backgroundGeneral: "#959595",
                backgroundImportante: "#403F40",
                fontColorDiscreto: "#7F7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#FFFFFF",
                fontColorGeneral: "#000000",
                selected: true
            },
            {
                backgroundNormal: "#FAFDFF",
                backgroundGeneral: "#d51b1b",
                backgroundImportante: "#B8DDFF",
                fontColorDiscreto: "#7D7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                fontColorGeneral: "#FFFFFF",
                selected: false
            },
            {
                backgroundNormal: "#FAFFFE",
                backgroundGeneral: "#ffac00",
                backgroundImportante: "#B8FFEA",
                fontColorDiscreto: "#7D7F7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                fontColorGeneral: "#FFFFFF",
                selected: false
            },
            {
                backgroundNormal: "#FFFFFA",
                backgroundGeneral: "#508ce3",
                backgroundImportante: "#FDFFB8",
                fontColorDiscreto: "#7F7F7D",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                fontColorGeneral: "#FFFFFF",
                selected: false
            }
        ]
    }
    var ast = {
        backgroundThemeColor: "#fff",
        backgroundSideThemeColor: "#f63440",
        backgroundSideOverThemeColor: "#ad262e",
        backgroundColor: "#454040",
        backgroundLoading: "white",
        backgroundLogin: 'Content/Images/coorp/FONDOS_LOGIN/AST.png',
        corpImage: dominio + "Content/Images/coorp/astfondosrojos.png",
        corpImage2: dominio + "Content/Images/coorp/LogoAstLogin.png",
        displayName: "AST",
        buttonColor: "#FFB031",
        fontRegular: "tlmarkAst-regular",
        fontBold: "tlmarkAst-bold",
        fontLight: "tlmarkAst-light",
        ambientes: [
            {
                backgroundNormal: "#FFFBFA",
                backgroundGeneral: "#f63440",
                backgroundImportante: "#E03D2D",
                fontColorDiscreto: "#7F7E7D",
                fontColorNormal: "#000000",
                fontColorImportante: "#FFFFFF",
                selected: true
            },
            {
                backgroundNormal: "#FAFDFF",
                backgroundGeneral: "#ac0069",
                backgroundImportante: "#B8DDFF",
                fontColorDiscreto: "#7D7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FAFFFE",
                backgroundGeneral: "#3497f6",
                backgroundImportante: "#B8FFEA",
                fontColorDiscreto: "#7D7F7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FFFFFA",
                backgroundGeneral: "#ff6d21",
                backgroundImportante: "#FDFFB8",
                fontColorDiscreto: "#7F7F7D",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            }
        ]
    }
    var b12 = {
        backgroundThemeColor: "#FFFFFF",
        backgroundColor: "#1b2d3f",
        backgroundSideThemeColor: "#00b5df",
        backgroundSideOverThemeColor: "#007c99",
        backgroundLoading: "#1b2d3f",
        backgroundLogin: 'Content/Images/coorp/FONDOS_LOGIN/B12.png',
        corpImage: dominio + "Content/Images/coorp/b12_blanco.png",
        corpImage2: dominio + "Content/Images/coorp/b12-apaisado.svg",
        displayName: "B12",
        buttonColor: "#00E19F",
        fontRegular: "B12-regular",
        fontBold: "B12-bold",
        fontLight: "B12-light",
        ambientes: [
            {
                backgroundNormal: "#FFFBFF",
                backgroundGeneral: "#00b5df",
                backgroundImportante: "#403F40",
                fontColorDiscreto: "#7F7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#FFFFFF",
                selected: true
            },
            {
                backgroundNormal: "#FAFDFF",
                backgroundGeneral: "#f34860",
                backgroundImportante: "#B8DDFF",
                fontColorDiscreto: "#7D7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FAFFFE",
                backgroundGeneral: "#ffb031",
                backgroundImportante: "#B8FFEA",
                fontColorDiscreto: "#7D7F7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FFFFFA",
                backgroundGeneral: "#62c127",
                backgroundImportante: "#FDFFB8",
                fontColorDiscreto: "#7F7F7D",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            }
        ]
    }
    var rockethall = {
        backgroundThemeColor: "#FFFFFF",
        backgroundColor: "#363936",
        backgroundSideThemeColor: "#ff6b00",
        backgroundSideOverThemeColor: "#ba4e00",
        backgroundLoading: "#1b2d3f",
        backgroundLogin: 'Content/Images/coorp/FONDOS_LOGIN/KOSMOS_ROCKETHALL.png',
        corpImage: dominio + "Content/Images/coorp/rockethall_fondosNaranjas.png",
        corpImage2: dominio + "Content/Images/coorp/rockethall.png",
        displayName: "RocketHall",
        buttonColor: "#FF6B00",
        fontRegular: "rockethall-regular",
        fontBold: "rockethall-bold",
        fontLight: "rockethall-light",
        ambientes: [
            {
                backgroundNormal: "#FFFFFF",
                backgroundGeneral: "#ff6b00",
                backgroundImportante: "#D7282F",
                fontColorDiscreto: "#000000",
                fontColorNormal: "#000000",
                fontColorImportante: "#FFFFFF",
                selected: true
            },
            {
                backgroundNormal: "#FAFDFF",
                backgroundGeneral: "#d7282f",
                backgroundImportante: "#B8DDFF",
                fontColorDiscreto: "#7D7E7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FAFFFE",
                backgroundGeneral: "#535353",
                backgroundImportante: "#B8FFEA",
                fontColorDiscreto: "#7D7F7F",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            },
            {
                backgroundNormal: "#FFFFFA",
                backgroundGeneral: "#959595",
                backgroundImportante: "#FDFFB8",
                fontColorDiscreto: "#7F7F7D",
                fontColorNormal: "#000000",
                fontColorImportante: "#000000",
                selected: false
            }
        ]
    }

    //TLMARK || AST || B12
    function getTheme(empresa) {
        if (empresa == "TLMARK") {
            return tlmark;
        }
        else if (empresa == "AST") {
            return ast;
        }
        else if (empresa == "B12") {
            return b12;
        }
        else if (empresa == "ROCKETHALL") {
            return rockethall;
        }
        else {
            return theme;
        }
    }

    var interfaz = {
        theme: theme,
        tlmark: tlmark,
        ast: ast,
        b12: b12,
        getTheme: getTheme
    }
    return interfaz;

})
