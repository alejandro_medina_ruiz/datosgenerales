﻿app.factory("UtilFactory", function () {
    var interfaz = {};

    var dateToString = function (fecha) {

        fecha = fecha.substr(6, fecha.length);
        fecha = fecha.substr(0, fecha.length - 2)

        fechaDate = new Date(parseInt(fecha))

        return to2char(fechaDate.getDate()) + "/" + to2char((fechaDate.getMonth() + 1)) + "/" + fechaDate.getFullYear();
    }
    var dateToDate = function (fecha) {
        fecha = fecha.substr(6, fecha.length);
        fecha = fecha.substr(0, fecha.length - 2)

        fechaDate = new Date(parseInt(fecha))
        return fechaDate;
    }
    var stringDate = function (fechaDate) {
        return to2char(fechaDate.getDate()) + "/" + to2char((fechaDate.getMonth() + 1)) + "/" + fechaDate.getFullYear();
    }
    var onlyNumbers = function (value) {
        var out = "";
        if (value !== undefined) {
            var filtro = "0123456789"
            for (var i = 0; i < value.length; i++) {
                if (filtro.indexOf(value.charAt(i)) !== -1) {
                    out += value.charAt(i);
                }
            }
        }

        if (out.length == 0) {
            out = undefined
        }
        return out
    }

    var formatMoney = function (number, moneda) {
        if (moneda == "COP") {
            return number.toFixed(3);
        }
        if (moneda == "SOL") {
            return number.toFixed(2);
        }
        if (moneda == "EUR") {
            return number.toFixed(2);
        }
        if (moneda == "USD") {
            return number.toFixed(2);
        }
    }

    var to2char = function (number) {
        if (number < 10) {
            return "0" + number;
        }
        else {
            return number;
        }
    }

    interfaz = {
        dateToString: dateToString,
        dateToDate: dateToDate,
        stringDate: stringDate,
        onlyNumbers: onlyNumbers,
        to2char: to2char,
        formatMoney: formatMoney
    };

    return interfaz
});