﻿<div style="display:none" id="settings">
</div>
<div style="position:absolute; z-index:9999; background-color:#50505020; width:100%; height:100%;" layout="column" layout-align="center center" ng-if="loading">

    <div class="preloader" id="waittingModal">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="7" stroke-miterlimit="10" />
        </svg>
    </div>
</div>

<div style="width:100%; height:100%;" layout="column" layout-align="center center" id="contenedor">
</div>