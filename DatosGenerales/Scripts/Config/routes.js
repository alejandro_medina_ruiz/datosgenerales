app.config(function($stateProvider, $locationProvider, $ocLazyLoadProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise(function($injector) {
		var $state = $injector.get("$state");
		$state.go('list');
	});

	$stateProvider
		.state("list", {
			url: "/",
			templateUrl: dominio + 'Pages/List/list.html',
			controller: 'listCtrl',
			params: {
				dato: null
			},
			resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					// you can lazy load files for an existing module
					// return $ocLazyLoad.load('homeCtrl.js');
					return $ocLazyLoad.load([
						dominio + 'Scripts/Controllers/list.js',
						dominio + 'Scripts/Controllers/dialogForm.js',
						dominio + 'Static/validate/validate.js'
					]);
				}]
			}
		})
});
