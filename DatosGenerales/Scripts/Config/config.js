﻿
app.config(function ($mdThemingProvider) {

    $mdThemingProvider.definePalette('tlmark', {
        '50': '000000',
        '100': '000000',
        '200': '000000',
        '300': '000000',
        '400': '000000',
        '500': '000000',
        '600': '000000',
        '700': '000000',
        '800': '000000',
        '900': '000000',
        'A100': '000000',
        'A200': '000000',
        'A400': '000000',
        'A700': '000000',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
        // on this palette should be dark or light

        'contrastDarkColors': ['50'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });
    $mdThemingProvider.definePalette('ast', {
        '50': 'FF0000',
        '100': 'FF0000',
        '200': 'FF0000',
        '300': 'FF0000',
        '400': 'FF0000',
        '500': 'FF0000',
        '600': 'FF0000',
        '700': 'FF0000',
        '800': 'FF0000',
        '900': 'FF0000',
        'A100': 'FF0000',
        'A200': 'FF0000',
        'A400': 'FF0000',
        'A700': 'FF0000',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
        // on this palette should be dark or light

        'contrastDarkColors': ['50'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });
    $mdThemingProvider.definePalette('accent', {
        '50': 'FFFA87',
        '100': 'FFFA87',
        '200': 'FFFA87',
        '300': 'FFFA87',
        '400': 'FFFA87',
        '500': 'FFFA87',
        '600': 'FFFA87',
        '700': 'FFFA87',
        '800': 'FFFA87',
        '900': 'FFFA87',
        'A100': 'FFFA87',
        'A200': 'FFFA87',
        'A400': 'FFFA87',
        'A700': 'FFFA87',
        'contrastDefaultColor': 'dark',


        'contrastDarkColors': ['50', '100',
         '200', '300', '400', 'A100'],
        'contrastLightColors': undefined
    });

    if (empresa == 'TELEMARK') {
        $mdThemingProvider.theme('default')
      .primaryPalette('tlmark')
      .accentPalette('accent')
        .warnPalette('orange');
    }
    else {
        $mdThemingProvider.theme('default')
      .primaryPalette('ast')
      .accentPalette('accent')
      .warnPalette('orange');
    }
});

app.config(function ($mdDateLocaleProvider) {
    // Establecemos con moment el tiempo a la zona Española
    moment.locale('es_ES', {
        week: {
            dow: 1 // Lunes es el primer dia de la semana
        }
    });
    moment.locale('es');

    //Declaramos los nombres de Meses y dias para el datepicker de Google
    $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    $mdDateLocaleProvider.shortMonths = ['Ene', 'Feb', 'Marz', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sept', 'Oct', 'Nov', 'Dic'];
    $mdDateLocaleProvider.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $mdDateLocaleProvider.shortDays = ['D', 'L', 'M', 'X', 'J', 'V', 'S'];

    // Las semanas empiezan en lunes
    $mdDateLocaleProvider.firstDayOfWeek = 1;

    //Usamos Moment para darle formato al datepicker
    $mdDateLocaleProvider.parseDate = function (dateString) {
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $mdDateLocaleProvider.formatDate = function (date) {
        var m = moment(date);
        return m.isValid() ? m.format('L') : '';
    };
});
app.config(
        function ($controllerProvider, $provide, $compileProvider) {
            
            app._controller = app.controller;
            app._service = app.service;
            app._factory = app.factory;
            app._value = app.value;
            app._directive = app.directive;
            app._component = app.component;
            app.controller = function (name, constructor) {
                $controllerProvider.register(name, constructor);
                return (this);
            };
            app.service = function (name, constructor) {
                $provide.service(name, constructor);
                return (this);
            };
            app.factory = function (name, factory) {
                $provide.factory(name, factory);
                return (this);
            };
            app.value = function (name, value) {
                $provide.value(name, value);
                return (this);
            };
            app.directive = function (name, factory) {
                $compileProvider.directive(name, factory);               
                return (this);
            };
            
            app.component = function (name, options) {
                $compileProvider.component(name, options);          
                return (this);
            }
        }
    );

app.directive('iosDblclick',
    function () {

        const DblClickInterval = 300; //milliseconds

        var firstClickTime;
        var waitingSecondClick = false;

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                
                element.bind('click', function (e) {
                    if (!waitingSecondClick) {
                        firstClickTime = (new Date()).getTime();
                        waitingSecondClick = true;

                        setTimeout(function () {
                            waitingSecondClick = false;
                        }, DblClickInterval);
                    }
                    else {
                        waitingSecondClick = false;

                        var time = (new Date()).getTime();
                        if (time - firstClickTime < DblClickInterval) {
                            scope.$apply(attrs.iosDblclick);
                            
                        }
                    }
                });
            }
        };
    });