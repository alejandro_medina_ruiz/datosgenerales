﻿var contCOMPILES = 0;
app.factory("asyncFactory", function ($http, $compile, $timeout) {
    //Nos traemos el loading
    function init(callback) {
        $http.post(dominioAction + "/Home/getLogging").then(function (r) {
            var html = r.data.value;
            $("#contenedor").html = html;
            $("#contenedor").html(html.replace(/scripttag/g, 'script'));

            angular.element($("#contenedor")).injector().invoke(['$compile', function ($compile) {
                var $scope = angular.element($("#contenedor")).scope();

                $compile($("#contenedor"))($scope);
                try {
                    $scope.$apply();
                } catch (e) { };

                callback();
            }]);
        })
    }
    function load(data, id, callback) {
        var html = data;
        $("#"+id).html = html;
        $("#" + id).html(html.replace(/scripttag/g, 'script'));      
        invoke(id);           
        function invoke(id) {
            angular.element($("#" + id)).injector().invoke(['$compile', function ($compile) {
                var $scope = angular.element($("#" + id)).scope();                
                    $compile($("#" + id))($scope);             
                callback();
            }]);
        }
    }
    return {
        init: init,
        load:load
    }

});