app.controller('listCtrl', ['$scope', '$http', '$timeout', '$rootScope', 'themeFactory', '$mdDialog', '$state', function ($scope, $http, $timeout, $rootScope, themeFactory, $mdDialog, $state) {
	$scope.theme = themeFactory.getTheme(empresa);

	$scope.tecnico = usuario.rol == 'TECNICOS';


	$scope.obtenerCabeceras = function (white) {
		var cabecerasListaFija = {
				white : ['KPI', 'CAMPANIA','VALOR', 'DESCRIPCION', 'FECHA_INICIO', 'FECHA_FIN'],
				cabeceras: {'KPI':'KPI', 'CAMPANIA':'Campaña','VALOR':'Valor', 'DESCRIPCION':'Descripción', 'FECHA_INICIO':'Fecha de Inicio', 'FECHA_FIN':'Fecha de Fin'}
			},
			cabecerasLista = {
				white : ['KPI', 'VALOR', 'DESCRIPCION', 'FECHA_INICIO', 'FECHA_FIN'],
				cabeceras: {'KPI':'KPI', 'VALOR':'Valor', 'DESCRIPCION':'Descripción', 'FECHA_INICIO':'Fecha de Inicio', 'FECHA_FIN':'Fecha de Fin'}
			}
		if (white) {
			return $scope.modoBusqueda == 'MOVISTAR_FIJA' ? cabecerasListaFija.white : cabecerasLista.white ;
		}else {
			return $scope.modoBusqueda == 'MOVISTAR_FIJA' ? cabecerasListaFija.cabeceras : cabecerasLista.cabeceras ;
		}
	};

	$scope.esFija = function () {
	  return $scope.modoBusqueda == 'MOVISTAR_FIJA';
	};


	$scope.initCtr = function () {
	  $scope.fechaIni = new Date();
	  $scope.fechaFin = new Date();
	  $rootScope.obtenerLista = $scope.obtenerLista;
	  $scope.obtenerLista();
	};

	$scope.optionesModoBusqueda = [
        { value: 'MOVISTAR_FIJA', name: "Movistar Fija" }, { value: 'HIBU', name: "HIBU" }, { value: 'MOVISTAR_DIGITAL', name: "Movistar Digital" }
    ];

	$scope.cambiarBusqueda = function(modo) {
		$scope.modoBusqueda = modo;
		$scope.obtenerLista();
	}

	$scope.modoBusqueda = 'MOVISTAR_FIJA';
	/**
	 * @name obtenerLista
	 * @desciption esta Función va al back-end y trae los datos que necesitemos,
	 *       en este ejemplo va al controlador Ejemplo.vb a la función obtenerLista,
	 */
	$scope.obtenerLista = function () {
      $scope.list = [];
	  if ( usuario.rol == 'TECNICOS' ) {
	  	usuario.proyecto = $scope.modoBusqueda;
	  }
      $rootScope.apiRun({ url: "api/Datos/ObtenerLista", method: "POST",
         data: {fechaIni: $scope.fechaIni, fechaFin: $scope.fechaFin, proyecto:usuario.proyecto } })
      .then(function(response) {
		  console.log('response list');
		  console.log(response);
		  $scope.list = response.data.list;
	      $scope.$apply();
      })
      .catch(function (error) {
		  // $scope.toaster('Así para notificación exitosa', 7, true, true, 'green');
		  $scope.toaster(error.data.message, 7, true, true, 'red');
      });
    };




	/**
     * @name crearVisita
     * @desciption Esta función nos muestra un dialogo, para este ejemplo se llama de tres lados,
	 *       1: boton "+nuevo" : se debe poner como directiva en la etiqueta tmk-table así: nueva="crearVisita"
	 *       2: boton "editar" : se debe poner como directiva en la etiqueta tmk-table así: editar="crearVisita"
     *       3: Doble click sobre registro : se debe poner como directiva en la etiqueta tmk-table así: view="crearVisita"
	 * @params ev = Evento (Lo envía el componente), dato = Lo que sea que mandemos en el click del check,
	 *    	para este ejemplo se envia todo el registro seleccionado, así:" $parent.dato.selectAction($parent.dato) "
     */
	$scope.crearVisita = function (ev, dato) {
	  if ( usuario.rol == 'TECNICOS' ) {
  	  	usuario.proyecto = $scope.modoBusqueda;
  	  }
      if (dato && dato.length > 1 ) { $scope.toaster('No se puede editar mas de un registro.', 7, false, true);return; };
	  	$state.params.dato = dato;
	  	$mdDialog.show({
	      // controller: DialogController,
	      templateUrl: $scope.dominio + "Pages/List/dialogForm.html",
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:false,
		  escapeToClose:false,
	      fullscreen: false // Only for -xs, -sm breakpoints.
	    })
        // $scope.dialoguer(
        //   $scope.dominio + "Pages/List/dialogForm.html",
        //   {
        //       title: 'Formulario de Registro',
        //       subtitle: dato && dato.length ? 'Editar Existente' : 'Crear Nuevo',
        //       icon: dominio + 'Content/Icons/run.svg',
        //       height:'85%',
        //       outDisable: true
        //   },
        //   {
        //       theme: $scope.theme,
        //       ambiente: $scope.ambiente,
        //       confirmer: $scope.confirmer,
        //       toaster: $scope.toaster,
        //       modePequenio: $scope.modePequenio,
        //       editar: dato,
        //       obtenerLista:$scope.obtenerLista
        //   }
	  	// );
    };



	$scope.initCtr();
}]);
