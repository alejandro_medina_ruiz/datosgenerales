app.controller('dialogFormCtrl', ['$scope', '$http', '$timeout', '$rootScope', 'themeFactory', '$mdDialog', '$state', '$mdToast', function ($scope, $http, $timeout, $rootScope, themeFactory, $mdDialog, $state, $mdToast) {
    $scope.theme = themeFactory.getTheme(empresa);

    var aux = {};
    // Este for siempre debe estar en los dialogos
    // for (key in $scope.$parent.$ctrl.dialogBag) {
    //     $scope[key] = $scope.$parent.$ctrl.dialogBag[key];
    // }


    $scope.form = { FECHA_INICIO: new Date(), FECHA_FIN: new Date(), };

    $scope.initCtr = function () {
        console.log('$state.params');
		$scope.obtenerLista = $state.params.obtenerLista;
        console.log($state.params.dato);
        if ($state.params.dato && $state.params.dato.length) {
            $scope.form = $state.params.dato[0];
            // $scope.form.FECHA_INICIO = moment( $scope.form.FECHA_INICIO, "DD/MM/YYYY" );
            // $scope.form.FECHA_FIN = moment( $scope.form.FECHA_FIN, "DD/MM/YYYY" );
            $scope.form.FECHA_INICIO = new Date( $scope.form.FECHA_INICIO.split('/').reverse().join('/') );
            $scope.form.FECHA_FIN = new Date( $scope.form.FECHA_FIN.split('/').reverse().join('/') );
        };
        $scope.obtenerOpciones();
    };

    // esta función cierra el dialogo
    $scope.descartar = function () {
        $rootScope.obtenerLista();
        $mdToast.hide();
        $mdDialog.hide();
        // $scope.CloseDialog();
    };

    // Los dialogos tambien tienen eventos
    // $scope.dialogEvents.onCloseDialog = function () {
    //     $scope.obtenerLista();
    // }

    /**
     * @name obtenerOpciones
     * @desciption esta Función va al back-end y trae los datos que necesitemos,
     *       en este ejemplo va al controlador Ejemplo.vb a la función obtenerOpciones,
     */
    $scope.obtenerOpciones = function () {
      $rootScope.apiRun({ url: "api/Datos/ObtenerOpciones", method: "POST", data:{ proyecto:usuario.proyecto } })
      .then(function(response) {
          console.log('response obtenerOpciones');
          console.log(response);
          $scope.opciones = response.data.list;
          $scope.campanias = response.data.campanias;
          // $scope.toaster('Así para notificación exitosa', 7, true, true, 'green');
          // $scope.toaster('Así para notificación no exitosa', 7, true, true, 'red');
          $scope.$apply();
      })
      .catch(function (error) {
          console.log('error');
          console.log(error);
        // $scope.toaster(error.data.message, 7, true, true, 'red');
        var toast = $mdToast.simple()
            .textContent('Marked as read')
            .actionKey('z')
            .actionHint('Press the Control-"z" key combination to ')
            .action('X')
            .dismissHint('Activate the Escape key to dismiss this toast.')
            .highlightAction(true)
            // Accent is used by default, this just demonstrates the usage.
            .highlightClass('md-accent')
            .position("bottom, right")
            .hideDelay(0);

          $mdToast.show(toast);
      });
    };

    $scope.requiereCampania = function () {
      return usuario.proyecto == 'MOVISTAR_FIJA';
    };


    $scope.guardar = function() {
        console.log($scope.form);
        $scope.errores = validate($scope.form, _validar);
        console.log('$scope.errores');
        console.log($scope.errores);
        if (usuario.proyecto == "MOVISTAR_FIJA" && !$scope.form.CAMPANIA) {
            $scope.errores ? $scope.errores.CAMPANIA = ["*El campo 'CAMPAÑA' es requerido."] : $scope.errores = { CAMPANIA : ["*El campo 'CAMPAÑA' es requerido."]};
        }
        if ( !$scope.errores ) {
            $rootScope.apiRun({ url: "api/Datos/Guardar", method: "POST", data : {form: JSON.stringify($scope.form), proyecto:usuario.proyecto }  })
            .then(function(response) {
                console.log('response obtenerOpciones');
                console.log(response);
                $rootScope.obtenerLista();
              //   var toast = $mdToast.simple()
              //       .textContent(error.data.message)
              //       .hideDelay(0);
              //
              // $mdToast.show(toast);
				$mdDialog.hide();
                $scope.$apply();
            })
            .catch(function (error) {
                console.log('error');
                console.log(error);
                // $scope.toaster(error.data.message, 7, true, true, 'red');
                var toast = $mdToast.simple()
                    .textContent(error.data.message)
                    .actionKey('z')
                    .actionHint('Press the Control-"z" key combination to ')
                    .action('X')
                    .dismissHint('Activate the Escape key to dismiss this toast.')
                    .highlightAction(true)
                    // Accent is used by default, this just demonstrates the usage.
                    .highlightClass('md-accent')
                    .position("bottom, right")
                    .hideDelay(0);

              $mdToast.show(toast);
            });
        }
    }

    var _validar = {
        KPI: {
          presence: {
            value:true,
            message: "^El campo 'KPI' es requerido."
          }
        },
        VALOR: {
          presence: {
                value:true,
                message: "^El campo 'VALOR' es requerido."
            },
            format: {
              pattern: /^[0-9.,]+$/,
              message: "^No se permiten caracteres especiales"
            }
        },
        DESCRIPCION: {
          presence: {
            value:true,
            message: "^El campo 'DESCRIPCION' es requerido."
          }
        },
    }

    $scope.initCtr();
}]);
// 'KPI','VALOR', 'DESCRIPCION',
