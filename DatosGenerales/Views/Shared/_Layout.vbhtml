﻿﻿
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@ViewBag.Title</title>
	@Styles.Render("~/Content/css")
	@Scripts.Render("~/bundles/modernizr")
	<script>
		var empresa = 'TELEMARK',
			dominioAction = '@ViewData("dominio")',
			dominio = '@ViewData("dominioRecur")',
			host = '@ViewData("Host")',
			debug = ('@ViewData("debug")' == 'True'),
			usuario;
		// var usuario = JSON.parse('@ViewData("Usuario")');
	</script>
	<link href="~/Static/angular-material/angular-material.css" rel="stylesheet" />
	<link href="~/Static/bootstrap/bootstrap.css" rel="stylesheet" />
	<link href="~/Content/Styles/Site.css" rel="stylesheet" />
</head>


<body ng-app="myApp" ng-controller="main" ng-cloak>
	<div layout="row" layout-align="center center" ng-if="loading" style="width:100%; height:100%; overflow:hidden;">

		<md-progress-circular class="md-warn md-hue-1" md-diameter="96"></md-progress-circular>
	</div>
	<div style="width:100%; height:100%; overflow:hidden;">
		@RenderBody()
	</div>


	@Scripts.Render("~/bundles/jquery")

	<script src="~/Static/angular/angular.js"></script>
	<script src="~/Static/angular/angular-aria.js"></script>
	<script src="~/Static/angular/angular-message.js"></script>
	<script src="~/Static/angular/angular-animate.js"></script>
	<script src="~/Static/angular/angular-route.js"></script>
	<script src="~/Static/angular/angular-resource.js"></script>
	<script src="~/Static/moment/moment.js"></script>
	<script src="~/Static/moment/moment-with-locales.min.js"></script>
	<!-- <script src="~/Static/underscore/underscore.js"></script> -->
	<script src="~/Static/angular-material/angular-material.js"></script>
	<script src="~/Static/angular/angular-ui-router.js"></script>
	<script src="~/Static/angular/ocLazyLoadRequire.js"></script>
	<script src="~/Static/angular/ocLazyLoad.js"></script>
	<!-- <script src="~/Static/validate/validate.js"></script> -->

	<script src="~/Scripts/Config/app.js"></script>
	<script src="~/Scripts/Config/routes.js"></script>
	<script src="~/Scripts/Factories/theme.js"></script>
	<script src="~/Scripts/Factories/util.js"></script>
	<script src="~/Scripts/Factories/async.js"></script>
	<script src="~/Scripts/Config/config.js"></script>
	<script src="~/Scripts/Controllers/main.js"></script>

</body>

</html>