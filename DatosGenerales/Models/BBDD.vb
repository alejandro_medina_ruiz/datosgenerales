﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.DirectoryServices
Imports System.Security.Principal
Imports DatosGenerales.ActiveDirectory


Public Class BBDD
    Private conexion_DIGI As SqlConnection
    Private conexion_FIJA As SqlConnection
    Private conexion_HIBU As SqlConnection
    Sub New()
        conexion_DIGI = New SqlConnection(ConfigurationManager.ConnectionStrings("BBDD_DIGI").ConnectionString)
        conexion_HIBU = New SqlConnection(ConfigurationManager.ConnectionStrings("BBDD_HIBU").ConnectionString)
        conexion_FIJA = New SqlConnection(ConfigurationManager.ConnectionStrings("BBDD_FIJA").ConnectionString)
    End Sub

    Public Function InstansciarDirectorioActivo() As DirectoryEntry
        Dim entrarDirectorio As New DirectoryEntry
        Dim dom = ObtenerDominio()
        entrarDirectorio = EntrarDirectorioActivo(dom)
        Return entrarDirectorio
    End Function

    'Public Function compruebaUser(user As String, pass As String) As logResult
    '    Dim comandoCheck As New SqlCommand
    '    comandoCheck = getComando()
    '    comandoCheck.CommandText = "sp_administrators_colision"

    '    comandoCheck.Parameters.AddWithValue("@inKey", user)
    '    comandoCheck.Parameters.AddWithValue("@inPass", pass)

    '    comandoCheck.Connection.Open()

    '    Dim logResult As logResult = New logResult()

    '    Dim resultado As SqlDataReader = comandoCheck.ExecuteReader()
    '    If resultado.HasRows Then
    '        'MsgBox(resultado.FieldCount)
    '        While (resultado.Read())
    '            'MsgBox(resultado.Item(1))
    '            Dim usuario As New usuario()
    '            usuario.id = resultado.Item(0)
    '            usuario.User = resultado.Item(1)
    '            usuario.Email = resultado.Item(2)
    '            usuario.DisplayName = resultado.Item(3)
    '            usuario.Corporativo = resultado.Item(4)
    '            usuario.defPass = resultado.Item(5)
    '            logResult.result = True
    '            logResult.usuario = usuario
    '            logResult.status = "Success"
    '            comandoCheck.Connection.Close()
    '            Return logResult
    '        End While
    '        logResult.status = "Contraseña Incorrecta"
    '        comandoCheck.Connection.Close()
    '        Return logResult
    '    Else
    '        logResult.status = "Usuario Incorrecto"
    '        comandoCheck.Connection.Close()
    '        Return logResult
    '    End If
    'End Function

    'Public Function usuarioValido(usuario As String) As Object
    '    Dim comandoCheck As New SqlCommand
    '    comandoCheck = getComando()
    '    comandoCheck.CommandText = "usuarioValido"

    '    comandoCheck.Parameters.AddWithValue("@usuario", usuario)

    '    comandoCheck.Connection.Open()

    '    Dim logResult As Boolean = False

    '    Dim resultado As Object = comandoCheck.ExecuteScalar()
    '    comandoCheck.Connection.Close()
    '    Return resultado
    'End Function

    'Public Function compruebaUserCorp(user As String) As logResult
    '    Dim comandoCheck As New SqlCommand
    '    comandoCheck = getComando()

    '    comandoCheck.CommandType = CommandType.StoredProcedure
    '    comandoCheck.CommandText = "sp_administrators_colision_corp"

    '    comandoCheck.Parameters.AddWithValue("@inKey", user)

    '    comandoCheck.Connection.Open()

    '    Dim logResult As logResult = New logResult()

    '    Dim resultado As SqlDataReader = comandoCheck.ExecuteReader()
    '    If resultado.HasRows Then
    '        'MsgBox(resultado.FieldCount)
    '        While (resultado.Read())
    '            'MsgBox(resultado.Item(1))
    '            Dim usuario As New usuario()
    '            usuario.id = resultado.Item(0)
    '            usuario.User = resultado.Item(1)
    '            usuario.Email = resultado.Item(2)
    '            usuario.DisplayName = resultado.Item(3)
    '            usuario.Corporativo = resultado.Item(4)
    '            usuario.defPass = resultado.Item(5)
    '            logResult.result = True
    '            logResult.usuario = usuario
    '            logResult.status = "Success"
    '            comandoCheck.Connection.Close()
    '            Return logResult
    '        End While
    '        logResult.status = "Login Error"
    '        comandoCheck.Connection.Close()
    '        Return logResult
    '    Else
    '        logResult.status = "Usuario Incorrecto"
    '        comandoCheck.Connection.Close()
    '        Return logResult
    '    End If
    'End Function


    Protected Function getComando(ByVal proyecto As String) As SqlCommand
        Dim comando As New SqlCommand
        If proyecto = "HIBU" Then
            comando.Connection = conexion_HIBU
        ElseIf proyecto = "MOVISTAR_DIGITAL" Then
            comando.Connection = conexion_DIGI
        ElseIf proyecto = "MOVISTAR_FIJA" Then
            comando.Connection = conexion_FIJA
        Else
            Throw New Exception("Proyecto no permitido")
        End If
        'comando.Connection = conexion
        comando.CommandType = CommandType.StoredProcedure
        Return comando
    End Function


    Public Function obtenerLista(ByVal fechaini As Date, ByVal fechafin As Date, ByVal proyecto As String, Optional ByVal user_tel As String = "") As List(Of Datos)
        Dim comandoCheck As New SqlCommand
        comandoCheck = inicializarSPComando("SP_FRT_ObtenerDatos", proyecto)
        Dim lista As New List(Of Datos)
        'comandoCheck = inicializarSPComando("obtenerVisitas")
        Try
            comandoCheck.Parameters.Add(New SqlParameter("@fechaini", SqlDbType.Date) With {.Value = fechaini})
            comandoCheck.Parameters.Add(New SqlParameter("@fechafin", SqlDbType.Date) With {.Value = fechafin})
            comandoCheck.Parameters.Add(New SqlParameter("@user_tel", SqlDbType.NVarChar) With {.Value = user_tel})
            comandoCheck.Connection.Open()
            Dim adapter As SqlDataAdapter = New SqlDataAdapter(comandoCheck)
            adapter.TableMappings.Add("Table", "Datos")
            Dim resultadoConsulta As DataSet = New DataSet
            adapter.Fill(resultadoConsulta)
            comandoCheck.Connection.Close()
            If resultadoConsulta.Tables("Datos").Rows.Count > 0 Then
                For Each rowData As DataRow In resultadoConsulta.Tables("Datos").Rows
                    Dim dato As Datos = New Datos
                    dato.KPI = rowData.Field(Of String)("KPI")
                    dato.VALOR = rowData.Field(Of String)("VALOR")
                    dato.DESCRIPCION = rowData.Field(Of String)("DESCRIPCION")
                    dato.FECHA_INICIO = rowData.Field(Of DateTime)("FECHA_INICIO")
                    dato.FECHA_FIN = rowData.Field(Of DateTime)("FECHA_FIN")
                    If proyecto = "MOVISTAR_FIJA" Then
                        dato.CAMPANIA = rowData.Field(Of String)("CAMPANIA")
                    End If
                    dato.Id_MNL_OPE_FRT_DATOS_GENERALES = rowData.Field(Of Integer)("Id_MNL_OPE_FRT_DATOS_GENERALES")

                    lista.Add(dato)
                Next
            End If
        Catch ex As Exception
            comandoCheck.Connection.Close()
            lista = Nothing
            Throw New Exception("Error de validacion en: obtenerReporteRespuestas", ex)
        End Try
        Return lista
    End Function


    Public Function obtenerOpciones(ByVal Tipolista As String, ByVal proyecto As String, Optional ByVal id_dependencia As Integer = 0) As List(Of Object)
        Dim comandoCheck As New SqlCommand
        comandoCheck = inicializarSPComando("SP_FRT_ObtenerOpciones", proyecto)


        Dim lista As New List(Of Object)
        'comandoCheck = inicializarSPComando("obtenerVisitas")
        Try
            comandoCheck.Parameters.Add(New SqlParameter("@Lista", SqlDbType.NVarChar) With {.Value = Tipolista})
            comandoCheck.Parameters.Add(New SqlParameter("@id_dependencia", SqlDbType.Int) With {.Value = id_dependencia})
            comandoCheck.Connection.Open()
            Dim adapter As SqlDataAdapter = New SqlDataAdapter(comandoCheck)
            adapter.TableMappings.Add("Table", "Datos")
            Dim resultadoConsulta As DataSet = New DataSet
            adapter.Fill(resultadoConsulta)
            comandoCheck.Connection.Close()
            If resultadoConsulta.Tables("Datos").Rows.Count > 0 Then
                For Each rowData As DataRow In resultadoConsulta.Tables("Datos").Rows
                    Dim obj As Object = New With {
                        .descripcion = rowData.Field(Of String)("descripcion"),
                        .id = rowData.Field(Of Integer)("id")
                    }
                    lista.Add(obj)
                Next
            End If
        Catch ex As Exception
            comandoCheck.Connection.Close()
            lista = Nothing
            Throw New Exception("Error de validacion en: obtenerReporteRespuestas", ex)
        End Try
        Return lista
    End Function




    Public Function GuardarDato(ByVal dato As Datos, ByVal proyecto As String) As Integer
        Dim resultado As Integer = 0
        Dim duplicado As Integer = 0
        Dim comandocheck As New SqlCommand
        Try

            If dato.Id_MNL_OPE_FRT_DATOS_GENERALES = 0 Then
                duplicado = validarRegistros(dato, proyecto)
                If duplicado > 0 Then
                    Throw New Exception("Ya se encuentra un registro en ese rango de fechas.")
                End If

            End If

            comandocheck = getComando(proyecto)
            comandocheck.CommandType = CommandType.StoredProcedure
            comandocheck.Connection.Open()

            If proyecto = "MOVISTAR_FIJA" Then
                comandocheck.Parameters.AddWithValue("@CAMPANIA", dato.CAMPANIA)
            End If

            comandocheck.Parameters.AddWithValue("@Id_MNL_OPE_FRT_DATOS_GENERALES", dato.Id_MNL_OPE_FRT_DATOS_GENERALES)
            comandocheck.Parameters.AddWithValue("@KPI", dato.KPI)
            comandocheck.Parameters.AddWithValue("@VALOR", dato.VALOR)
            comandocheck.Parameters.AddWithValue("@DESCRIPCION", dato.DESCRIPCION)

            comandocheck.Parameters.AddWithValue("@FECHA_INICIO", Convert.ToDateTime(dato.FECHA_INICIO))
            comandocheck.Parameters.AddWithValue("@FECHA_FIN", Convert.ToDateTime(dato.FECHA_FIN))

            comandocheck.CommandText = "SP_FRT_GuardarDato"
            Try
                resultado = comandocheck.ExecuteScalar
                If resultado = -1 Then
                    Throw New Exception("Ya se encuentra un registro en ese rango de fechas.")
                End If
            Catch ex As Exception
                comandocheck.Connection.Close()
                Throw ex
            End Try
        Catch ex As Exception
            If duplicado > 0 Then
                Throw ex
            End If
            comandocheck.Connection.Close()
            Throw ex
        End Try
        comandocheck.Connection.Close()
        Return resultado
    End Function


    Public Function validarRegistros(ByVal dato As Datos, ByVal proyecto As String) As Integer
        Dim comandocheck As New SqlCommand
        Try
            comandocheck = getComando(proyecto)
            comandocheck.CommandType = CommandType.StoredProcedure
            comandocheck.Connection.Open()

            If proyecto = "MOVISTAR_FIJA" Then
                comandocheck.Parameters.AddWithValue("@CAMPANIA", dato.CAMPANIA)
            End If
            comandocheck.Parameters.AddWithValue("@KPI", dato.KPI)
            comandocheck.Parameters.AddWithValue("@FECHA_INICIO", Convert.ToDateTime(dato.FECHA_INICIO))
            comandocheck.Parameters.AddWithValue("@FECHA_FIN", Convert.ToDateTime(dato.FECHA_FIN))

            comandocheck.CommandText = "SP_FRT_validarDatosExistentes"
            Dim resultado = comandocheck.ExecuteScalar

            'If resultado > 0 Then
            '    Throw New Exception("Ya se encuentra un registro en ese rango de fechas.")
            'End If
            comandocheck.Connection.Close()
            Return resultado
        Catch ex As Exception
            comandocheck.Connection.Close()
            Throw New Exception("Error de validacion, ya existe un registro con el mismo rango de fechas.")
        End Try
    End Function

    Public Function inicializarSPComando(ByVal spName As String, ByVal proyecto As String) As SqlCommand
        Dim comando As New SqlCommand
        Try
            If proyecto = "HIBU" Then
                comando.Connection = conexion_HIBU
            ElseIf proyecto = "MOVISTAR_DIGITAL" Then
                comando.Connection = conexion_DIGI
            ElseIf proyecto = "MOVISTAR_FIJA" Then
                comando.Connection = conexion_FIJA
            Else
                Throw New Exception("Proyecto no permitido")
            End If
            comando.CommandText = spName
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = Integer.MaxValue
            Return comando
        Catch ex As Exception
            Throw New Exception("Error de validacion en: getSPComando", ex)
        End Try
        Return comando
    End Function


End Class
