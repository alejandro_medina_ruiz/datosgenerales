﻿Imports System.Net.Http
Imports Newtonsoft.Json
Imports DatosGenerales.HomeController
Imports System.Web.Http
Imports System.Net

Public Class DatosController
    Inherits ApiController
    Private datos As New BBDD()
    Public Shared infoUsuario As Dictionary(Of String, String)
    Private usuarioLogueado As String


#Region "FUNCIONALIDADES CRUD"

    <HttpPost>
    Function ObtenerLista(request As HttpRequestMessage, data As Newtonsoft.Json.Linq.JObject) As Object
        Try
            Dim fechaIni As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("fechaIni"), String)
            Dim fechaFin As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("fechaFin"), String)
            Dim proyecto As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("proyecto"), String)
            Return Ok(New With {
                        .list = datos.obtenerLista(fechaIni, fechaFin, proyecto)
                    })
        Catch ex As Exception
            Return request.CreateResponse(HttpStatusCode.BadRequest, (New With {.status = 401, .message = "Datos erroneos"}))
        End Try

        'Return resultado
    End Function


    <HttpPost>
    Function ObtenerOpciones(request As HttpRequestMessage, data As Newtonsoft.Json.Linq.JObject) As Object
        Dim proyecto As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("proyecto"), String)

        Try
            Return Ok(New With {
                        .list = datos.obtenerOpciones("KPI", proyecto),
                        .campanias = datos.obtenerOpciones("CAMPA", proyecto)
                    })
        Catch ex As Exception
            Return request.CreateResponse(HttpStatusCode.BadRequest, (New With {.status = 401, .message = "Datos erroneos"}))
        End Try

        'Return resultado
    End Function

    <HttpPost>
    Function Guardar(request As HttpRequestMessage, data As Newtonsoft.Json.Linq.JObject) As Object
        Dim form As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("form"), String)
        Dim proyecto As String = CType(data.ToObject(Of Dictionary(Of String, Object)).Item("proyecto"), String)
        Dim result = 0
        Try
            Dim dato = JsonConvert.DeserializeObject(Of Datos)(form)
            result = datos.GuardarDato(dato, proyecto)
            If (result = 0) Then
                Return request.CreateResponse(HttpStatusCode.BadRequest, (New With {.status = 401, .message = "No se creo el registro por favor valide nuevamente"}))
            End If
            Return request.CreateResponse(HttpStatusCode.Created, (New With {.status = 201, .message = "Ejemplo creada correctamente", .id = result}))
        Catch ex As Exception
            Return request.CreateResponse(HttpStatusCode.InternalServerError, (New With {.status = 500, .err = True, .message = ex.Message.ToString()}))
        End Try
    End Function

#End Region



End Class
