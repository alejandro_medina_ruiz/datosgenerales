﻿Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Dim debug As Boolean = ConfigurationManager.AppSettings("debug")

    Function Index(Optional id As String = "") As ActionResult
        Dim dominio As String = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Split("?")(0)
#If DEBUG Then
        dominio = "https://" & dominio.Substring(7)
#Else
        dominio = "http://" & dominio.Substring(7)
#End If



        '''IMPORTANTE DESCOMENTAR SI SE EJECUTA EN ENTORNO CON SSL

        'If (System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Contains("http://")) Then
        '    Response.Redirect(dominio)
        'End If
        Dim dominioRecur As String
        If Not id.Equals("") Then
            dominioRecur = dominio.Substring(0, dominio.LastIndexOf(id))
        Else
            dominioRecur = dominio
        End If


        If (id.Equals("")) Then
            '
            '
            If (dominio.ToUpper.Contains("TLMARK")) Then
                id = "TLMARK"
            ElseIf (dominio.ToUpper.Contains("AST")) Then
                id = "AST"
            ElseIf (dominio.ToUpper.Contains("B12")) Then
                id = "B12"
            ElseIf (dominio.ToUpper.Contains("ROCKETHALL")) Then
                id = "ROCKETHALL"
            Else
                id = ConfigurationManager.AppSettings("Default")
            End If

            dominio += "/" & id & "/"
        Else
            If Not (id.ToUpper.Equals("TLMARK") Or
                    id.ToUpper.Equals("AST") Or
                    id.ToUpper.Equals("B12") Or
                    id.ToUpper.Equals("ROCKETHALL")) Then
                'HAY UNA ID QUE NO ES EMPRESA POR LO QUE INSTANCIAMOS LA VARIABLE MODULO
                ViewData("modulo") = id.ToUpper
                'Reconstruimos el ID de empresa segun su dominio
                If (dominio.ToUpper.Contains("TLMARK")) Then
                    id = "TLMARK"
                ElseIf (dominio.ToUpper.Contains("AST")) Then
                    id = "AST"
                ElseIf (dominio.ToUpper.Contains("B12")) Then
                    id = "B12"
                ElseIf (dominio.ToUpper.Contains("ROCKETHALL")) Then
                    id = "ROCKETHALL"
                Else
                    id = ConfigurationManager.AppSettings("Default")
                End If
            End If
        End If

        'If (debug) Then
        'dominio = "http://localhost:58917/" + id + "/"
        'End If

        ViewData("debug") = debug
        'ViewData("dominio") = dominio


        Dim dominioBase As String = dominio.Substring(0, dominio.LastIndexOf(".com/") + 5)
        ViewData("dominioRecur") = dominioRecur
        ViewData("Title") = "Datos Generales"
        ViewData("empresa") = id.ToUpper
        ViewData("Host") = dominioBase

        If id.ToUpper.Equals("TELEMARK") Then
            ViewData("icono") = dominioRecur & "faviconTlmark.ico"
        ElseIf id.ToUpper.Equals("AST") Then
            ViewData("icono") = dominioRecur & "faviconAST.ico"
        ElseIf id.ToUpper.Equals("ROCKETHALL") Then
            ViewData("icono") = dominioRecur & "rockethall.ico"
        ElseIf id.ToUpper.Equals("B12") Then
            ViewData("icono") = dominioRecur & "b12.ico"
        End If

        Return View()
    End Function
End Class
