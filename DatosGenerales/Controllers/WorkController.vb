﻿Imports System.Net
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Web.Http
Imports System.IO
Imports System.Threading
Imports System.Security.Claims
Imports Newtonsoft.Json
Imports System.Net.Mail
Imports DatosGenerales.ActiveDirectory

Namespace Controllers
    Public Class WorkController
        Inherits ApiController
        'Private datos As New CapaDatos()

        <HttpPost>
        Function loggin() As Object
            Return Json(Of Object)(New With {.value = getData("Media/parts/panelPrincipal.html")})
        End Function

        <HttpGet>
        Function getLogging() As Object
            Return Json(Of Object)(New With {.value = getData("Media/parts/panelLoggin.html"), .status = 1})
        End Function

        <HttpGet>
        Function getPC() As Object
            Dim identity As ClaimsIdentity = HttpContext.Current.User.Identity

            Dim admin As Boolean = identity.Claims(2).Value
            If (admin) Then
                Return Json(Of Object)(New With {.value = getData("Media/parts/panelControl.html")})
            Else
                Return Json(Of Object)(New With {.value = "Unauthorized"})
            End If
        End Function

        <HttpGet>
        Function getConfig() As Object
            Return Json(Of Object)(New With {.value = getData("Media/parts/Configuracion.html")})
        End Function

        <HttpGet>
        Function getHelp() As Object
            Return Json(Of Object)(New With {.value = getData("Media/parts/ayuda.html")})
        End Function

        <HttpGet>
        Function getHome() As Object
            Return Json(Of Object)(New With {.value = getData("Pages/home.html"), .usuario = JsonConvert.SerializeObject(Access())})
        End Function

        <HttpGet>
        Function getError() As Object
            Return Json(Of Object)(New With {.value = getData("Pages/error.html")})
        End Function

        <HttpGet>
        Function getModulo() As Object
            Return Json(Of Object)(New With {.value = getData("Media/parts/" + Request.GetQueryNameValuePairs.ToList(0).Key + ".html")})
        End Function

        Protected Function getData(path As String) As String
            Return My.Computer.FileSystem.ReadAllText(HttpContext.Current.Server.MapPath("~/" & path))
        End Function

        Private Function getURLData(URL As String) As String
            Dim Request As WebRequest = WebRequest.Create(URL)

            Request.Credentials = CredentialCache.DefaultCredentials
            Dim Response As WebResponse = Request.GetResponse()
            Dim dataStream As Stream = Response.GetResponseStream()
            Dim reader As StreamReader = New StreamReader(dataStream)
            Dim respuesta As String = reader.ReadToEnd()

            reader.Close()
            Response.Close()

            Return respuesta
        End Function

        <HttpGet>
        Public Function getComponents() As Object
            Dim idApp As Integer = ConfigurationManager.AppSettings("ID_CORE")
            Dim apiURL As String = ConfigurationManager.AppSettings("CORE_URL")
            Return Newtonsoft.Json.JsonConvert.DeserializeObject(Of Object)(getURLData(apiURL & "Worker/getComponents?app=" & idApp))
        End Function


        Function Access() As Dictionary(Of String, String)
            Try
                Dim data As Dictionary(Of String, String) = CargarInfoUsuario()
                If Not IsNothing(data) Then
                    Return data
                Else
                    Throw New Exception("No se encontro usuario")
                End If
            Catch ex As Exception
                Throw New Exception("No se encontro usuario")
            End Try

        End Function


    End Class
End Namespace