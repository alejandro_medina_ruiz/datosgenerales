﻿Imports System.DirectoryServices
Imports System.Security.Claims
Imports System.Security.Principal
Partial Class ActiveDirectory
    Inherits ClaimsPrincipal

    Public Const BACKOFFICE As String = "BACKOFFICE"
    Public Const SUPERVISORES As String = "SUPERVISORES"
    Public Const TELEOPERADORES As String = "TELEOPERADORES"
    ' En web.config colocar las siguientes lineas en el app settings para obtener el dominio
    ' <!-- Dominios asignar el valor según dominio
    '  value="ast.local"
    '  value="telemark-spain.com" -->
    ' <add key = "dominioAst" value="" />
    ' <add key = "dominioTlmark" value="telemark-spain.com" />
    ' <!--Para controlar si el dominio es coorporativo
    '  value="S" si es corporativa
    '  value="N" si no es corporativa-->
    ' <add key = "esCorporativa" value="N" />
    Public Shared tmpDominio As String = ObtenerDominioTmp()

    ''' <summary>
    ''' Obtiene el dominio asignado
    ''' recordar vaiar variable config
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function ObtenerDominioTmp() As String
        ObtenerDominioTmp = ""
        Try
            Dim ast As String = ConfigurationManager.AppSettings.Item("dominioAst")
            Dim tlamrk As String = ConfigurationManager.AppSettings.Item("dominioTlmark")
            'Siempre debe de estar un vacia
            If Not String.IsNullOrEmpty(ast) Then
                ObtenerDominioTmp = ast
            End If
            If Not String.IsNullOrEmpty(tlamrk) Then
                ObtenerDominioTmp = tlamrk
            End If

        Catch ex As Exception
            Throw New Exception("Error!, ObtenerDominioTmp", ex)
        End Try

        Return ObtenerDominioTmp

    End Function

    'Propiedades
    Public Shared ReadOnly Property UsuarioDA As String
        Get
            Return ObtenerUsuarioDirectorioActivo()
        End Get
    End Property

    Public Enum Proyectos
        BackOffice_P
        Supervisores_P
        Teleoperadores_P
    End Enum

    ''' <summary>
    ''' Obtiene el usuario del directorio activo
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function ObtenerUsuarioDirectorioActivo() As String
        ObtenerUsuarioDirectorioActivo = ""
        Try
#If DEBUG Then
            ObtenerUsuarioDirectorioActivo = Security.Principal.WindowsIdentity.GetCurrent.Name.Split("\")(1)
            'ObtenerUsuarioDirectorioActivo = "sup05dg3"
            'ObtenerUsuarioDirectorioActivo = "sup05jv10"
            'ObtenerUsuarioDirectorioActivo = "tec05br1"
            'ObtenerUsuarioDirectorioActivo = "sup05fd1"
            'ObtenerUsuarioDirectorioActivo = "sup05jo2"
            'ObtenerUsuarioDirectorioActivo = "sup05jv2"
#Else
            'Utils.escribeLog("Entra a obtener usuario de sesion" + HttpContext.Current.User.Identity.Name)
            ObtenerUsuarioDirectorioActivo = HttpContext.Current.User.Identity.Name.Split("\")(1)
            'Utils.escribeLog("usuario AD Release: " + HttpContext.Current.User.Identity.Name)
#End If
        Catch ex As Exception
            'Utils.escribeLog("usuario: " + ex.ToString)
            Throw New Exception("Error! el directorio activo no retornó el usuario", ex)
        End Try
        Return ObtenerUsuarioDirectorioActivo
    End Function

    ''' <summary>
    ''' Retorna el objeto con los datos de infoUsuario
    ''' </summary>
    ''' <param name="usuarioPrueba">Recibe una cadena con el usuario (SOLO PARA PRUEBAS)</param>
    ''' <param name="dominioPrueba">Recibe una cadena con el dominio del Directorio Activo (SOLO PARA PRUEBAS)</param>
    ''' <returns></returns>
    Public Shared Function CargarInfoUsuario(ByVal Optional usuarioPrueba As String = "",
                                             ByVal Optional dominioPrueba As String = "") As Dictionary(Of String, String)
        CargarInfoUsuario = Nothing
        Dim tmpUsuario As String = ""
        Dim existeUsuario As New Object
        Try
            'Asigna el usuario (Pruebas o el que se obtuvo del DA)
            If Not String.IsNullOrEmpty(usuarioPrueba) Then
                tmpUsuario = usuarioPrueba
            Else
                tmpUsuario = UsuarioDA
            End If
            'Valida que se obtuvo el usuario del DA
            If String.IsNullOrEmpty(tmpUsuario) Then Return Nothing

            'Valida que el usuario exista en el directorio activo
            existeUsuario = ValidarUsuarioDirectorioActivo(tmpUsuario, dominioPrueba)


            If IsNothing(existeUsuario) Then Return Nothing

            If String.IsNullOrEmpty(existeUsuario("dominio")) Then Return Nothing

            'Inicializa el DA y le retorna los datos de infoUsuario
            'CargarInfoUsuario = InciarDirectorioActivo(IIf(String.IsNullOrEmpty(usuarioPrueba), UsuarioDA, usuarioPrueba),, dominioPrueba)
            CargarInfoUsuario = InciarDirectorioActivo(tmpUsuario,, existeUsuario("dominio"))
            If IsNothing(CargarInfoUsuario) Then Return Nothing

            'Valida que haya cargado por lo menos una propiedad
            If CargarInfoUsuario.Count = 0 Then Return Nothing

        Catch ex As Exception
            Throw New Exception("Error al obtener el usuario del directorio activo, comuniquese con el administrador", ex)
        End Try
        Return CargarInfoUsuario
    End Function

    ''' <summary>
    ''' Metodo main para instanciar el dominio por el cual se
    ''' accede a la aplicacion recibe por parametro el usuario
    ''' </summary>
    ''' <param name="usuario">Recibe el usuario del DA</param>
    ''' <param name="esDominioDinamico">Recibe True cuando el dominio es dinamico (Requiere mapeo de clase y BD)</param>
    ''' <param name="dominioPruebas">Recibe el nombre del dominio (SOLO PRUEBAS)</param>
    ''' <returns></returns>
    Public Shared Function InciarDirectorioActivo(ByVal usuario As String,
                                                  ByVal Optional esDominioDinamico As Boolean = False,
                                                  ByVal Optional dominioPruebas As String = "") As Dictionary(Of String, String)
        Dim nombreDominio As String
        Dim nombreUsuario As String
        Dim ciudadUsuario As String
        Dim proyectoUsuario As String
        Dim nombreGrupo As String
        Dim cedulaUsuario As String
        Dim entrarDirectorio As New DirectoryEntry
        Dim infoUsuario As Dictionary(Of String, String)

        InciarDirectorioActivo = Nothing

        Try
            If Not String.IsNullOrEmpty(usuario) Then
                '->Se asigna el nombre del dominio de donde proviene
                '->Si el dominio es dinamico se realiza a partir de una tabla de configuracion
                '  en la capa de datos.
                If Not esDominioDinamico Then
                    '->Si es dominioPruebas se asigna el que llegue por parametro (UNICAMENTE PARA PRUEBAS)
                    nombreDominio = ObtenerDominio(dominioPruebas)
                Else
                    nombreDominio = ObtenerDominio(dominioPruebas)
                    'nombreDominio = ObtenerDominioDinamico(usuario)
                End If

                'Establece el ingreso al directorio activo solo una vez
                'cuando se inicia session y se asigna recibe el nombre
                'del dominio (Validar que el dominio no este vacio)
                If Not String.IsNullOrEmpty(nombreDominio) Then

                    'Variable que contiene la configuración de ingreso al
                    'al directorio activo
                    entrarDirectorio = EntrarDirectorioActivo(nombreDominio)

                    'Se asigna el nombre completo del usuario que inicio la session
                    nombreUsuario = ObtenerNombreUsuario(entrarDirectorio, usuario)
                    If String.IsNullOrEmpty(nombreUsuario) Then Return Nothing

                    'Se asigna la ciudad del usuario que inicio la session
                    ciudadUsuario = ObtenerCiudadUsuario(entrarDirectorio, usuario)
                    If String.IsNullOrEmpty(ciudadUsuario) Then Return Nothing

                    'Se asigna la proyecto del usuario que inicio la session
                    proyectoUsuario = ObtenerProyectoUsuario(entrarDirectorio, usuario)
                    If String.IsNullOrEmpty(proyectoUsuario) Then Return Nothing

                    'Se asina la cedula del usuario como nueva validación, tomada del DA en el campo Telephone number
                    cedulaUsuario = ObtenerCedulaUsuario(entrarDirectorio, usuario, ciudadUsuario)
                    If String.IsNullOrEmpty(cedulaUsuario) Then Return Nothing

                    'Retorna el nombre del grupo al cual pertence el usuario
                    nombreGrupo = ObtenerUnidadOrganizacionalUsuario(entrarDirectorio, usuario)
                    If String.IsNullOrEmpty(nombreGrupo) Then Return Nothing

                    'Concatena el número de cédula del usuario
                    nombreGrupo = nombreGrupo & "," & cedulaUsuario

                    'Retorna el objeto con las propiedades que le corresponden al usuario
                    infoUsuario = ConstruirInfoUsuario(nombreGrupo, usuario)
                    If IsNothing(infoUsuario) Then Return Nothing

                    'Retorna Información de usuario
                    InciarDirectorioActivo = infoUsuario

                Else
                    Throw New Exception("Error al intentar establecer comunicación con el dominio, comuniquese con el administrador!")
                End If
            End If

        Catch ex As Exception
            Throw New Exception("Error al intentar obtener el usuario, por favor comuniquese con el administrador", innerException:=ex)
        End Try

        If IsNothing(InciarDirectorioActivo) Then
            Throw New Exception("Error al obtener la información del usuario en el directorio activo, comuniquese con el administrador!")
        End If

        Return InciarDirectorioActivo

    End Function



    '''' <summary>
    '''' Resetea la contraseña o desbloquea un usuario reciviendo como
    '''' parametros el usuario y la accion que se desea realizar
    '''' </summary>
    '''' <param name="usuario">Recibe el usuario del DA</param>
    '''' <param name="resetear">recive accion a reliazr resetear o desbloquear</param>
    '''' <returns></returns>
    'Public Shared Function ResetearContrasena(ByVal usuario As String, ByVal resetear As Boolean) As Boolean
    '    Dim proceso As Boolean = False
    '    Dim dominio As String

    '    Try
    '        dominio = ObtenerDominio()
    '        Dim entrada As New DirectoryEntry(dominio, "prtg", "wXj6WhT=p}", AuthenticationTypes.Secure)


    '        If Not String.IsNullOrEmpty(dominio) And Not IsNothing(entrada) Then
    '            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
    '            Dim resultado As SearchResultCollection
    '            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
    '            buscador.PropertiesToLoad.Add("userPrincipalName")
    '            resultado = buscador.FindAll()

    '            If Not IsNothing(resultado) Then

    '                For Each OneSearchResult As SearchResult In resultado
    '                    Dim AlterUser = OneSearchResult.GetDirectoryEntry()
    '                    If (resetear) Then
    '                        AlterUser.Invoke("SetPassword", "Tlm@rk2018")
    '                        AlterUser.Properties("pwdLastSet").Value = 0
    '                    End If
    '                    AlterUser.Properties("lockOutTime").Value = 0
    '                    AlterUser.CommitChanges()
    '                    AlterUser.Close()
    '                    proceso = True
    '                Next
    '            End If
    '        End If
    '    Catch ex As Exception
    '        proceso = False
    '    End Try
    '    Return proceso
    'End Function

    ''' <summary>
    ''' Obtiene el dominio desde el cual se inicio la aplicación
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function ObtenerDominio(ByVal Optional dominioPruebas As String = "") As String
        ObtenerDominio = ""
        Try
            'Validacion para probar dominios
            If Not String.IsNullOrEmpty(dominioPruebas) Then Return "LDAP://" + dominioPruebas

            'Abstrae del RootDSE el nombre del dominio actual
            Dim rootdse As New DirectoryServices.DirectoryEntry("LDAP://RootDSE")
            Dim nombre As String = rootdse.Properties("defaultNamingContext").Value.ToString()
            'Dim nombreDominio As String = nombre.ToLower.Replace("dc", "").Replace("=", "").Replace(",", ".")
            Dim nombreDominio As String = tmpDominio

            ObtenerDominio = "LDAP://" + nombreDominio

        Catch ex As Exception
            Throw ex
        End Try

        Return ObtenerDominio
    End Function

    ''' <summary>
    ''' Realiza la validación de ingreso al directorio activo
    ''' </summary>
    ''' <param name="dominio"></param>
    ''' <returns></returns>
    Public Shared Function EntrarDirectorioActivo(ByVal dominio As String) As DirectoryEntry
        Try
            Dim entrada As New DirectoryEntry(dominio)
            Return entrada
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene El Nombre Completo del Directorio Activo
    ''' espera por parametro el nombre del dominio y el
    ''' usuario de inicio de session
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <param name="usuario"></param>
    ''' <returns></returns>
    Public Shared Function ObtenerNombreUsuario(ByVal entrada As DirectoryEntry, ByVal usuario As String) As String
        ObtenerNombreUsuario = ""
        Try
            If String.IsNullOrEmpty(usuario) Then Return ObtenerNombreUsuario = ""

            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult
            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
            buscador.PropertiesToLoad.Add("name")
            buscador.PropertiesToLoad.Add("telephoneNumber")
            resultado = buscador.FindOne()

            If Not IsNothing(resultado) Then
                If Not resultado.Path.Contains("BAJA_USUARIOS") Then
                    ObtenerNombreUsuario = resultado.Properties("name").Item(0).ToString
                End If
            End If

        Catch ex As Exception
            Throw New Exception("Error al ObtenerNombreUsuario ", ex)
        End Try

        Return ObtenerNombreUsuario
    End Function

    ''' <summary>
    ''' Se extrae el numero de cedula del campo numero tele
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <param name="usuario"></param>
    ''' <returns></returns>
    Public Shared Function ObtenerCedulaUsuario(ByVal entrada As DirectoryEntry, ByVal usuario As String, ByVal esCentroColombia As String) As String
        ObtenerCedulaUsuario = ""
        Try
            If String.IsNullOrEmpty(usuario) Then Return ObtenerCedulaUsuario = ""
            Dim centro() As String = {"BOGOTA", "CALI", "PEREIRA"}

            'Valida el centro si es Colombia para extraer las cedulas en los desprendibles de nómina (Solo para Colombia)
            If Not centro.Contains(esCentroColombia) Then
                ObtenerCedulaUsuario = "NA"
                Return ObtenerCedulaUsuario
            End If

            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult
            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
            buscador.PropertiesToLoad.Add("telephoneNumber")
            resultado = buscador.FindOne()

            If Not IsNothing(resultado) Then
                If Not resultado.Path.Split(",").ElementAt(1).Contains("BAJA") Then
                    If resultado.Properties("telephoneNumber").Count > 0 Then
                        ObtenerCedulaUsuario = resultado.Properties("telephoneNumber").Item(0).ToString
                    End If
                End If
            End If

        Catch ex As Exception
            Throw New Exception("Error al ObtenerCedulaUsuario ", ex)
        End Try

        Return ObtenerCedulaUsuario
    End Function

    ''' <summary>
    ''' Obtiene La ciudad del Directorio Activo
    ''' espera por parametro el nombre del dominio y el
    ''' usuario de inicio de session
    ''' Nota: Para obtener la ciudad de todos los usuarios, se hace necesario recorrer la ciudad a la inversa, ya que
    ''' existen algunos usuarios con multiples funcionalidades en el directorio activo
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <param name="usuario"></param>
    ''' <returns></returns>
    Public Shared Function ObtenerCiudadUsuario(ByVal entrada As DirectoryEntry, ByVal usuario As String) As String
        ObtenerCiudadUsuario = ""
        Try
            If String.IsNullOrEmpty(usuario) Then Return ObtenerCiudadUsuario = ""

            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult
            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
            resultado = buscador.FindOne()

            If Not IsNothing(resultado) Then
                Dim res() As String = resultado.Path.Split(",")
                ObtenerCiudadUsuario = res(res.Length - 4).Substring(InStrRev(resultado.Path.Split(",")(2), "=")).Trim()

                'ObtenerCiudadUsuario = Split(resultado.Path, ",")(3).Substring(InStrRev(Split(resultado.Path, ",")(3).ToString, "="))
            End If

        Catch ex As Exception
            Throw New Exception("Error! en ObtenerCiudadUsuario. ", ex)
        End Try

        Return ObtenerCiudadUsuario
    End Function

    ''' <summary>
    ''' Obtiene el proyecto del Directorio Activo
    ''' espera por parametro el nombre del dominio y el
    ''' usuario de inicio de session
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <param name="usuario"></param>
    ''' <returns></returns>
    Public Shared Function ObtenerProyectoUsuario(ByVal entrada As DirectoryEntry, ByVal usuario As String) As String
        ObtenerProyectoUsuario = ""
        Try
            If String.IsNullOrEmpty(usuario) Then Return ObtenerProyectoUsuario = ""

            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult
            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
            resultado = buscador.FindOne()

            If Not IsNothing(resultado) Then
                If (resultado.Path.Split(",")(1).Substring(InStrRev(resultado.Path.Split(",")(1), "=")) <> "BAJA_USUARIOS") Then
                    ObtenerProyectoUsuario = resultado.Path.Split(",")(2).Substring(InStrRev(resultado.Path.Split(",")(2), "="))
                End If
            End If

        Catch ex As Exception
            Throw New Exception("Error! en ObtenerProyectoUsuario. ", ex)
        End Try

        Return ObtenerProyectoUsuario
    End Function

    ''' <summary>
    ''' Retorna la cadena
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <param name="usuario"></param>
    ''' <returns></returns>
    Public Shared Function ObtenerUnidadOrganizacionalUsuario(ByVal entrada As DirectoryEntry, ByVal usuario As String) As String
        ObtenerUnidadOrganizacionalUsuario = ""
        Try
            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult
            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
            resultado = buscador.FindOne()

            If IsNothing(resultado) Then Return ""

            Dim nombreUsuario As String = ""
            Dim proyecto As String = ""
            Dim rol As String = ""
            Dim llave As String = ""
            Dim centro As String = ""
            Dim ciudad As String = ""

            If Not String.IsNullOrEmpty(resultado.Path) And resultado.Path.Contains("OU") Then
                'Extrae dinamicamente la unidad a la que corresponde cada usuario
                Dim unidadOrganizacional As New List(Of String)
                'La primera posicion de la lista es la que contiene su proyecto o departamento
                unidadOrganizacional = ObtenerListaUnidadOrganizacionalUsuario(resultado.Path)

                nombreUsuario = resultado.Path.Split(",")(0).Substring(InStrRev(resultado.Path.Split(",")(0), "="))
                rol = resultado.Path.Split(",")(1).Substring(InStrRev(resultado.Path.Split(",")(1), "="))
                proyecto = unidadOrganizacional(1)
                'llave = "G_G_" & resultado.Path.Split(",")(4).Substring(InStrRev(resultado.Path.Split(",")(4), "=")).Substring(0, 2) & ""
                llave = "G_G_" & unidadOrganizacional(2).Substring(0, 2) & ""  'pendiente pam
                ciudad = unidadOrganizacional(unidadOrganizacional.Count - 2)
                ObtenerUnidadOrganizacionalUsuario = llave.Trim & "," & nombreUsuario.Trim() & "," & rol.Trim() & "," & proyecto.Trim() & "," & ciudad.Trim()
            End If

        Catch ex As Exception
            Throw New Exception("Ocurrió un error al obtener el grupo de usuario")
        End Try

        Return ObtenerUnidadOrganizacionalUsuario
    End Function


    ''' <summary>
    ''' Retorna una lista de cadenas de OU relacionadas con el usuario
    ''' Extrae dinamicamente la unidad a la que corresponde cada usuario
    ''' </summary>
    ''' <param name="resultadoRuta"></param>
    ''' <returns></returns>
    Public Shared Function ObtenerListaUnidadOrganizacionalUsuario(ByVal resultadoRuta As String) As List(Of String)
        ObtenerListaUnidadOrganizacionalUsuario = Nothing
        Dim unidadOrganizacional As New List(Of String)
        Try
            If Not String.IsNullOrEmpty(resultadoRuta) And resultadoRuta.Contains("OU") Then
                unidadOrganizacional = resultadoRuta.Split(",").Where(Function(m) m.Contains("OU")).ToList().Select(Function(i) i.Replace("OU=", "")).ToList()
                ObtenerListaUnidadOrganizacionalUsuario = unidadOrganizacional
            End If

        Catch ex As Exception
            Throw New Exception("Ocurrió un error ObtenerListaUnidadOrganizacionalUsuario", ex)
        End Try

        Return ObtenerListaUnidadOrganizacionalUsuario
    End Function


    ''' <summary>
    ''' Obtiene los usuarios a los que corresponde a un proyecto
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <param name="nombreGrupo"></param>
    ''' <returns></returns>
    Public Function ObtenerUsuariosPorProyecto(ByVal entrada As DirectoryEntry, ByVal nombreGrupo As String) As Collection
        ObtenerUsuariosPorProyecto = Nothing
        Try
            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim usuarioDelGrupo As New Collection()

            buscador.Filter = "(&(ObjectClass=group)(cn=" & nombreGrupo & "))"
            buscador.PropertiesToLoad.Add("Member")
            buscador.PropertiesToLoad.Add("distinguishedName")

            Dim resultado As SearchResult = buscador.FindOne()

            For Each usuario In resultado.Properties("Member")
                usuarioDelGrupo.Add(usuario)
            Next

        Catch ex As Exception
            ObtenerUsuariosPorProyecto = Nothing
        End Try

        Return ObtenerUsuariosPorProyecto

    End Function

    ''' <summary>
    ''' Valida si el usuario que inicia la sesion
    ''' se encuentra en el directorio activo
    ''' </summary>
    ''' <param name="usuario">Recibe el nombre del usuario que a validar</param>
    ''' <param name="dominioPruebas">Recibe el dominio a validar (SOLO PRUEBAS)</param>
    ''' <returns></returns>
    Public Shared Function ValidarUsuarioDirectorioActivo(ByVal usuario As String,
                                                          ByVal Optional dominioPruebas As String = "") As Object
        ValidarUsuarioDirectorioActivo = Nothing
        Try
            Dim corporativa As String = ""
            Dim nombreDominio As String
            Dim entrarDirectorio As New DirectoryEntry
            Dim existeUsuario As String
            Dim objDominio As New Dictionary(Of String, String)
            Dim accesoUrl As String
            ' Obtiene el dominio desde donde se accedió
            corporativa = ConfigurationManager.AppSettings.Item("esCorporativa")
            If corporativa.Contains("S") Then
                accesoUrl = HttpContext.Current.Request.Url.Host ' ponerla bien
                If accesoUrl.Contains("ast") Or accesoUrl.Contains("localhost") Then
                    dominioPruebas = "ast.local"
                Else
                    dominioPruebas = "telemark-spain.com"
                End If
            End If

            nombreDominio = ObtenerDominio(dominioPruebas)
            entrarDirectorio = EntrarDirectorioActivo(nombreDominio)
            existeUsuario = ObtenerNombreUsuario(entrarDirectorio, usuario)

            If String.IsNullOrEmpty(existeUsuario) Then
                ValidarUsuarioDirectorioActivo = Nothing
                Return ValidarUsuarioDirectorioActivo
            End If

            objDominio.Add("usuario", usuario)
            objDominio.Add("dominio", Replace(nombreDominio, "LDAP://", "").Trim())

            ValidarUsuarioDirectorioActivo = objDominio

        Catch ex As Exception
            Throw New Exception("Error al ValidarUsuarioDirectorioActivo. ", ex)
        End Try

        Return ValidarUsuarioDirectorioActivo
    End Function

    ''' <summary>
    ''' Retorna el objeto con los valores del usuario compuesto de:
    ''' LLave de la configuración, Proyecto al que corresponde
    ''' y el rol que desempeña
    ''' </summary>
    ''' <param name="nombreGrupo"></param>
    ''' <param name="usuario"></param>
    ''' <returns></returns>
    Public Shared Function ConstruirInfoUsuario(ByVal nombreGrupo As String, ByVal usuario As String) As Object
        ConstruirInfoUsuario = Nothing
        Try
            If Not String.IsNullOrEmpty(nombreGrupo) Then

                Dim objInfoUsuario As New Dictionary(Of String, String)
                Dim nombreUsuario As String = ""
                Dim proyecto As String = ""
                Dim rol As String = ""
                Dim llave As String = ""
                Dim ciudad As String = ""
                Dim cedula As String = ""

                llave = nombreGrupo.Split(",")(0)
                nombreUsuario = nombreGrupo.Split(",")(1)
                rol = nombreGrupo.Split(",")(2)
                proyecto = nombreGrupo.Split(",")(3)
                ciudad = nombreGrupo.Split(",")(4)
                cedula = nombreGrupo.Split(",")(5)

                objInfoUsuario.Add("llave", llave.ToString().Trim())
                objInfoUsuario.Add("usuario", usuario.ToString().Trim())
                objInfoUsuario.Add("nombreUsuario", nombreUsuario.ToString().Trim())
                objInfoUsuario.Add("proyecto", proyecto.ToString().Trim())
                objInfoUsuario.Add("rol", rol.ToString().Trim())
                objInfoUsuario.Add("ciudad", ciudad.ToString().Trim())
                objInfoUsuario.Add("cedula", cedula.ToString().Trim())

                ConstruirInfoUsuario = objInfoUsuario

            End If

        Catch ex As Exception
            Throw New Exception("Error al cargar el objeto con la información del usuario")
        End Try

        Return ConstruirInfoUsuario
    End Function


#Region "Funcionalidades de busqueda adicionales"

    Public Shared Function ObtenerUsuario(ByVal entrada As DirectoryEntry, ByVal nombreUsuario As String) As String
        ObtenerUsuario = ""
        Try
            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult

            buscador.Filter = "(&(objectClass=user)(name=" & nombreUsuario & "))"
            buscador.PropertiesToLoad.Add("sAMAccountName")
            resultado = buscador.FindOne()

            ObtenerUsuario = resultado.Properties("sAMAccountName").Item(0).ToString

            If String.IsNullOrEmpty(ObtenerUsuario) Then
                Throw New Exception("Error al obtener el nombre del usuario por favor comuniquese con el administrador")
            End If

        Catch ex As Exception
            Throw New Exception("Error en el método ObtenerUsuario", innerException:=ex)
        End Try

        Return ObtenerUsuario

    End Function

    Public Shared Function ObtenerListaSegunProyecto(ByVal infoUsuario As Dictionary(Of String, String),
                                                    ByVal proyecto As Proyectos) As List(Of Dictionary(Of String, String))

        ObtenerListaSegunProyecto = New List(Of Dictionary(Of String, String))
        Try
            Dim entrada As New DirectoryEntry
            entrada = EntrarDirectorioActivo(ObtenerDominio())
            Dim buscador As New System.DirectoryServices.DirectorySearcher(entrada)
            Dim resultado As System.DirectoryServices.SearchResult
            Dim nombreGrupo As String = ""
            Dim nombreProyectoGrupo As String = ""

            'Construye el nombre del Grupo según el rol del proyecto
            If Proyectos.BackOffice_P = proyecto Then
                nombreProyectoGrupo = BACKOFFICE
                nombreGrupo = infoUsuario.ElementAt(0).Value & "_" & infoUsuario.ElementAt(3).Value & "_" & nombreProyectoGrupo
            ElseIf Proyectos.Supervisores_P = proyecto Then
                nombreProyectoGrupo = SUPERVISORES
                nombreGrupo = infoUsuario.ElementAt(0).Value & "_" & infoUsuario.ElementAt(3).Value & "_" & nombreProyectoGrupo
            ElseIf Proyectos.Teleoperadores_P = proyecto Then
                nombreProyectoGrupo = TELEOPERADORES
                nombreGrupo = infoUsuario.ElementAt(0).Value & "_" & infoUsuario.ElementAt(3).Value & "_" & nombreProyectoGrupo
            End If

            buscador.Filter = "(&(ObjectClass=group)(cn=" & nombreGrupo & "))"
            buscador.PropertiesToLoad.Add("Member")
            resultado = buscador.FindOne()

            'Valida que el grupo exista
            If Not IsNothing(resultado) Then

                For Each usuario In resultado.Properties("Member")
                    If Split(usuario, ",")(0).Substring(InStrRev(Split(usuario, ",")(0).ToString, "=")) <> infoUsuario.ElementAt(2).Value Then

                        If Split(usuario, ",")(1).Substring(InStrRev(Split(usuario, ",")(1).ToString, "=")).ToUpper = nombreProyectoGrupo.ToUpper Then

                            Dim bo As New Dictionary(Of String, String)

                            bo.Add("llave", infoUsuario.ElementAt(0).Value.ToString)
                            bo.Add("usuario", ObtenerUsuario(entrada, Split(usuario, ",")(0).Substring(InStrRev(Split(usuario, ",")(0).ToString, "="))))
                            bo.Add("nombreUsuario", Split(usuario, ",")(0).Substring(InStrRev(Split(usuario, ",")(0).ToString, "=")))
                            bo.Add("proyecto", Split(usuario, ",")(2).Substring(InStrRev(Split(usuario, ",")(2).ToString, "=")))
                            bo.Add("rol", Split(usuario, ",")(1).Substring(InStrRev(Split(usuario, ",")(1).ToString, "=")))

                            ObtenerListaSegunProyecto.Add(bo)

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            Throw New Exception("Error en el método ObtenerListaBackOfficeProyectos", ex)
        End Try

        'If ObtenerListaSegunProyecto.Count = 0 Then
        '	Throw New Exception("El proyecto no possé usuarios BackOffice, comuniquese con el administrador")
        'End If

        Return ObtenerListaSegunProyecto

    End Function

    'Obtiene los grupos a donde pertenece cada usuario
    Public Shared Function ObtenerGrupoUsuario(ByVal entrada As DirectoryEntry, ByVal usuario As String) As String
        ObtenerGrupoUsuario = ""
        Try
            Dim buscador As DirectorySearcher = New DirectorySearcher(entrada.Path)
            buscador.Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" & usuario & "))"
            buscador.PropertiesToLoad.Add("memberOf")

            Dim nombreGrupo As StringBuilder = New StringBuilder()
            Dim resultado As SearchResult = buscador.FindOne()
            Dim conteoPropiedades As Integer = resultado.Properties("memberOf").Count

            Dim nombreRol As String
            Dim contador As Integer
            Dim indiceIgual As Integer
            Dim indiceComa As Integer
            Dim pertenece As String

            For contador = 0 To conteoPropiedades - 1
                'Obtiene la cadena con las rutas a la cual pertenece cada usuario
                nombreRol = CType(resultado.Properties("memberOf")(contador), String)

                If Not String.IsNullOrEmpty(nombreRol) Then
                    'Valida que la cadena contenga los datos del dominio
                    indiceIgual = nombreRol.IndexOf("=", 1)
                    indiceComa = nombreRol.IndexOf(",", 1)
                    pertenece = nombreRol.Substring((indiceIgual + 1), (indiceComa - indiceIgual) - 1)

                    If Not String.IsNullOrEmpty(pertenece) Then

                        'Cuenta la cantidad de veces que aparece en la pestaña MemberOf el separador "_"
                        'Según la estructura de configuración esta compuesto asi: ejemplo: "G_G_PE_BI"
                        '(Varía según el proyecto) -> consultar con tecnología
                        Dim conteoSeparadores As MatchCollection = Regex.Matches(pertenece, ("_"), RegexOptions.IgnoreCase)

                        'Si ell conteo es mayor que 3 quiere decir que el rol que le corresponde
                        'ejemplo: "G_G_PE_TECNOLOGIA_TECNICOS" despues del separador "_" 3 contiene
                        'Proyecto "TECNOLOGIA" y rol: TECNICOS
                        If conteoSeparadores.Count > 3 Then
                            'Valida usuario tecnologia
                            If pertenece.ToLower().Contains("tecnologia") Then
                                ObtenerGrupoUsuario = pertenece
                                Exit For
                            End If

                            'Valida usuario supervisor
                            If pertenece.ToLower().Contains("supervisor") Then
                                ObtenerGrupoUsuario = pertenece
                                Exit For
                            End If

                            'Valida usuario backoffice
                            If pertenece.ToLower().Contains("backoffice") Then
                                ObtenerGrupoUsuario = pertenece
                                Exit For
                            End If

                            'Valida usuario teleoperador
                            If pertenece.ToLower().Contains("teleoperador") Then
                                ObtenerGrupoUsuario = pertenece
                                Exit For
                            End If

                        End If

                    End If

                End If
            Next

        Catch ex As Exception
            Throw New Exception("Ocurrió un error al obtener el grupo de usuario")
        End Try
        Return ObtenerGrupoUsuario
    End Function

#End Region

End Class
