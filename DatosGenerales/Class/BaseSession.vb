﻿Imports System.Net

Public MustInherit Class BaseSession
	Inherits ControllerBase

	''' <summary>
	''' Propiedad de captura y solo lectura con la caracteristicas de session 
	''' </summary>
	''' <returns></returns>
	Public Shared ReadOnly Property SharedSession As System.Web.SessionState.HttpSessionState
		Get
			Return System.Web.HttpContext.Current.Session
		End Get
	End Property

	''' <summary>
	''' Constructor para almacenar datos de session y compartir entre controladores
	''' se asigna la fecha y hora para recuperar la session en caso de necesitar
	''' se asigna un tiempo de duración de las sessiones expresados en minutos
	''' para en este caso es de 7200 min
	''' (Para modificar el tiempo de session modificar en el Web.config el tiempo en la linea)
	''' => <sessionState mode="InProc" cookieless="false" timeout="7200" />
	''' </summary>
	Public Sub BaseSession()
		SharedSession("fechaActializada") = DateTime.Now
		SharedSession.Timeout = 7200
	End Sub

	Sub New()
		MyBase.New()
		BaseSession()
	End Sub




End Class
