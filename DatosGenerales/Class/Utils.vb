﻿Imports System.Configuration
Imports System.Globalization
Imports System.IO
Imports System.Net
Imports System.Net.Http
Imports System.Text

Public Class Utils
    Private Shared sw As TextWriter = Nothing
    Public Function soapCall(endPoint As String, soapMethod As String, soapAction As String, xml As String) As String
        'Dim req As HttpWebRequest = HttpWebRequest.Create(endPoint)
        Dim req As WebRequest = WebRequest.Create(endPoint)
        req.Headers.Add("SOAPMethodName", soapMethod)
        req.Headers.Add("SOAPAction", soapAction)
        req.ContentType = "text/xml"
        req.Method = "POST"
        req.Timeout = 20000

        Dim stm As Stream = req.GetRequestStream()

        Using (stm)
            Dim stmW As StreamWriter = New StreamWriter(stm)
            Using (stmW)
                stmW.Write(xml)
            End Using
        End Using

        Try
            'Protocolo de seguridad para los Ws https
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12

            Dim resp As WebResponse = req.GetResponse()
            Dim reader = New StreamReader(resp.GetResponseStream())
            Return reader.ReadToEnd()
        Catch ex As WebException
            If ex.Response IsNot Nothing Then
                If ex.Response.ContentLength <> 0 Then
                    Using stream = ex.Response.GetResponseStream()
                        Using reader = New StreamReader(stream)
                            Dim respuestaOnError As String = reader.ReadToEnd()
                            Console.WriteLine(respuestaOnError)
                            Try
                                System.Xml.Linq.XDocument.Parse(respuestaOnError)
                                Return respuestaOnError
                            Catch exOnNonXML As Exception
                                Throw New WebException(respuestaOnError, ex)
                            End Try
                        End Using
                    End Using
                End If
            Else
                Throw ex
            End If
        End Try

        Return ""
    End Function

    Public Function restCall(endPoint As String, contentType As String, method As String, body As String) As String
        Dim req As HttpWebRequest = HttpWebRequest.Create(endPoint)
        req.ContentType = contentType
        req.Method = method
        req.Timeout = 20000

        Dim stm As Stream = req.GetRequestStream()

        Using (stm)
            Dim stmW As StreamWriter = New StreamWriter(stm)
            Using (stmW)
                stmW.Write(body)
            End Using
        End Using

        Try
            Dim resp As HttpWebResponse = req.GetResponse()
            Dim reader = New StreamReader(resp.GetResponseStream())
            Return reader.ReadToEnd()
        Catch ex As WebException
            If ex.Response IsNot Nothing Then
                If ex.Response.ContentLength <> 0 Then
                    Using stream = ex.Response.GetResponseStream()
                        Using reader = New StreamReader(stream)
                            Dim respuestaOnError As String = reader.ReadToEnd()
                            Try
                                System.Xml.Linq.XDocument.Parse(respuestaOnError)
                                Return respuestaOnError
                            Catch exOnNonXML As Exception
                                Throw New WebException(respuestaOnError, ex)
                            End Try
                        End Using
                    End Using
                End If
            Else
                Throw ex
            End If
        End Try

        Return ""
    End Function

    Public Function restCallPostXml(endPoint As String, body As String) As String
        Return restCall(endPoint, "application/xml", "POST", body)
    End Function



    Public Shared Sub escribeLog(ByVal mensaje As String, Optional ByVal instancia As String = "GENERAL")

        Try

            'sw = TextWriter.Synchronized(New StreamWriter(System.Configuration.ConfigurationSettings.AppSettings.Get("rutaLog") & Year(Date.Now) & Format(Month(Date.Now), "D2") & Format(Day(Date.Now), "D2") & "_log_" & instancia & ".txt", True))

            sw = TextWriter.Synchronized(New StreamWriter(ConfigurationManager.AppSettings("rutaLog") & "log_" & instancia & ".txt", True))

            SyncLock sw
                sw.WriteLine(Date.Now & " --- " & mensaje)
                sw.Flush()
                sw.Close()
            End SyncLock

        Catch ex As Exception
            sw.Close()
            escribeLog("Error al escribir en log: " & ex.ToString & " ------ " & mensaje & " -------- " & instancia, "WRITER")
        End Try

    End Sub

    'Comprueba si la hora actual está dentro del horario de ejecución
    Public Function enHorario() As Boolean
        Dim enhora As Boolean = False
        Try
            'Dim horaActual As Integer = Convert.ToInt32(Format(Hour(Date.Now), "D2"))
            Dim horaActual As Integer = Date.Now.Hour
            Dim horaInicio As Integer = 0
            Dim horaFin As Integer = 0

            'Elijo un horario de ejecución en función del día de la semana
            If (Date.Now.DayOfWeek = DayOfWeek.Tuesday Or Date.Now.DayOfWeek = DayOfWeek.Saturday) Then
                horaInicio = ConfigurationManager.AppSettings("horaInicio")
                horaFin = ConfigurationManager.AppSettings("horaFin")
            End If

            'Si está dentro del horario o la variable de siempreCorriendo está a 1, devuelvo True
            If horaInicio > 0 And horaFin > 0 Then
                If (horaActual >= horaInicio And horaActual < horaFin) Then
                    enhora = True
                Else
                    enhora = False
                End If
            Else
                enhora = False
                Utils.escribeLog("No es el día de ejecución")

            End If
        Catch ex As Exception
            Utils.escribeLog("Entró en error  --- " + ex.Message)
        End Try
        Return enhora

    End Function
End Class
